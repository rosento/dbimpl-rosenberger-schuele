#include <iostream>
#include <ostream>
#include <string>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>

#include "definitions.hpp"

#include "Storage.hpp"


Storage::Storage()
{
	bits4Segment=BITS_SEG;
	bits4Page=BITS_PAGE;
	anzahlSeitenPS=pow(2,bits4Page); //anzahl Seiten pro Segment
}

Storage::~Storage()
{

}

uint64_t Storage::vordereKBits(uint64_t zahl, unsigned k)
{
	return zahl >> (64-k);
}

uint64_t Storage::hintereKBits(uint64_t zahl, unsigned k)
{
	uint64_t maske = 0xFFFFFFFFFFFFFFFF;
	/*generiere maske mit 0..01..1 mit k 1en*/
	maske = maske >> (64-k);
	return zahl & maske;
}
/** data: Zeiger auf bereits allokierten Bereich, der befuellt wird
*/
void Storage::readPage(uint64_t pageOffset, uint64_t segID, void *data)
{
	std::string dateiname = std::to_string(segID) + ".bin";
	
	int fd = open(dateiname.c_str(),O_RDONLY);
	/*existiert die Datei noch nicht?*/
	if( fd < 0){
		std::cerr << "Lege neue Datei an" << std::endl;
		fd = open(dateiname.c_str(),O_RDONLY|O_CREAT, S_IRUSR|S_IWUSR);
		posix_fallocate(fd,pageOffset*PAGE_SIZE,PAGE_SIZE);
		if (fd < 0){
			std::cerr << "Konnte Datei nicht oeffnen" << std::endl;
			exit(-1);
		}

		//fuelle mit 0 auf
		char *dataC = (char *) data;
		for(unsigned i=0; i < PAGE_SIZE; i++){
			dataC[i]=0;
		}
	}else{
		/*Lese die Datei in BufferFrame, hole den Zeiger
		  Dann berechne die Position:
		  P0 		P1 		P2		...
		  PAGE_SIZE	PAGE_SIZE	PAGE_SIZE	...
		*/
		pread(fd,data,PAGE_SIZE,pageOffset*PAGE_SIZE);
	}

	close(fd);
}

/*vordere Bits ergeben das Segment */
void Storage::readPage(uint64_t pageID, void *data)
{
	readPage(hintereKBits(pageID,bits4Page),
			vordereKBits(pageID,bits4Segment),
			data);
}

void Storage::writePage(uint64_t pageOffset, uint64_t segID, void *data)
{
	std::string dateiname = std::to_string(segID) + ".bin";
	int fd = open(dateiname.c_str(),O_RDWR);
	/*existiert die Datei noch nicht?*/
	if( fd < 0){
		std::cerr << "Lege neue Datei an" << std::endl;
		fd = open(dateiname.c_str(),O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
	}
	if (fd < 0){
		std::cerr << "Konnte Datei nicht oeffnen" << std::endl;
		exit(-1);
	}

	posix_fallocate(fd,pageOffset*PAGE_SIZE,PAGE_SIZE);
	pwrite(fd,data,PAGE_SIZE,pageOffset*PAGE_SIZE);

	close(fd);
	
}

void Storage::writePage(uint64_t pageID, void *data)
{
	return writePage(hintereKBits(pageID,bits4Page),
			vordereKBits(pageID,bits4Segment), data);
}

void Storage::deleteSegment(uint64_t segID)
{
	std::string dateiname = std::to_string(segID) + ".bin";
	if( remove(dateiname.c_str()) < 0)
		std::cerr << "Fehler beim Loeschen von " << segID << std::endl;
}


void Storage::clearStorage()
{
}

