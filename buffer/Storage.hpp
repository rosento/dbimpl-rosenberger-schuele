#ifndef STORAGE_HPP
#define STORAGE_HPP

#include <cstdint>
#include <cstdlib>

class Storage
{
  public:
  	Storage();
  	~Storage();
	void readPage(uint64_t pageOffset, uint64_t segID, void *data);
	void readPage(uint64_t pageID, void *data);
	void writePage(uint64_t pageOffset, uint64_t segID, void *data);
	void writePage(uint64_t pageID, void *data);
	void deleteSegment(uint64_t segID);
	void clearStorage();
  	uint64_t vordereKBits(uint64_t zahl, unsigned k);
  	uint64_t hintereKBits(uint64_t zahl, unsigned k);

  private:
  	uint64_t anzahlSeitenPS;
	unsigned bits4Segment, bits4Page;

};

#endif
