#ifndef BUFFERFRAME_HPP
#define BUFFERFRAME_HPP

#include <mutex>
#include <cstdint>
#include <cstdlib>

#include "Storage.hpp"

class BufferFrame
{
  public:
  	BufferFrame(uint64_t);
  	BufferFrame();
  	~BufferFrame();
	void *getData();
	bool isLatched();
	bool isDirty();
	void setDirty(bool);
	void lock(bool);
	void unlock();
	void setPageNo(uint64_t);
	uint64_t getPageNo();

  private:
  	uint64_t pageNo, lsn;
	bool dirty, latched;
	unsigned readingThreads;
	void *data;
	std::mutex mtx;
	Storage storage;
};

#endif
