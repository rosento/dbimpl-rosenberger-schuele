#ifndef TWOQUEUE_HPP
#define TWOQUEUE_HPP

#include <list>
#include <unordered_map>

class TwoQueue {
	public:
		TwoQueue();
		~TwoQueue();

		// Add a frame which is not in the TwoQueue yet.
		// DO NOT call this with frames already in the TwoQueue:
		void add(BufferFrame *frame);

		// Mark a frame already in the TwoQueue as used again.
		// ONLY call this with elements already in the queue.
		void again(BufferFrame *frame);

		// Remove the next unfixed page from the TwoQueue.
		// Returns the pageId of the removed page, or NULL if all are fixed.
		BufferFrame *remove();
	
	private:
		std::list<BufferFrame *> fifo, lru;
		std::unordered_map< BufferFrame*, std::list<BufferFrame *>::iterator > fifo_iterators, lru_iterators;
};

#endif
