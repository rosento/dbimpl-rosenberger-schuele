#include <iostream>

#include "BufferFrame.hpp"
#include "TwoQueue.hpp"


TwoQueue::TwoQueue() {
}

TwoQueue::~TwoQueue() {

}

void TwoQueue::add(BufferFrame *frame) {
	fifo_iterators[frame] = fifo.insert(fifo.end(), frame);
}

void TwoQueue::again(BufferFrame * frame) {
	// remove from fifo queue, if applicable
	std::unordered_map<BufferFrame*, std::list<BufferFrame*>::iterator >::iterator i = fifo_iterators.find(frame);
	if (i != fifo_iterators.end()) {
		fifo.erase(i->second);
		fifo_iterators.erase(i);
	}

	// remove from lru queue, if applicable
	std::unordered_map<BufferFrame*, std::list<BufferFrame*>::iterator >::iterator j = lru_iterators.find(frame);
	if (j != lru_iterators.end()) {
		lru.erase(j->second);
		lru_iterators.erase(j);
	}
	
	// add to lru queue
	lru_iterators[frame] = lru.insert(lru.end(), frame);
}

BufferFrame *TwoQueue::remove() {
	if (! fifo.empty()) {
        	// find element to be removed
		std::list<BufferFrame*>::iterator i = fifo.begin();
		while (i != fifo.end() && (*i)->isLatched()) {
			i++;
		}
		if (i != fifo.end()) {
			//remove element
			BufferFrame* result = *i;
			fifo.erase(i);
			fifo_iterators.erase(result);
			return result;
		}
	}

	// find element to be removed
	std::list<BufferFrame*>::iterator i = lru.begin();
	while (i != lru.end() && (*i)->isLatched()) {
		i++;
	}
	if (i == lru.end()) {
		return NULL;
	}

	//remove element
	BufferFrame* result = *i;
	lru.erase(i);
	lru_iterators.erase(result);
	return result;
}

