#ifndef BUFFERMANAGER_HPP
#define BUFFERMANAGER_HPP

#include <cstdint>

#include <map>
#include <mutex>
#include <exception>

#include "BufferFrame.hpp"
#include "Storage.hpp"

#include "TwoQueue.hpp"

class BufferFrame;

class EvictableException: public std::exception
{
	virtual const char* what() const throw()
    	{
        	return "No evictable page found.";
	}
};

class BufferManager
{
  public:
  	BufferManager(unsigned pageCount);
	BufferFrame& fixPage(uint64_t pageId, bool exclusive);
	void unfixPage(BufferFrame& frame, bool isDirty);
	~BufferManager();

  private:
  	unsigned pagesInMem;
	Storage storage;
	std::map<uint64_t,BufferFrame*> frames;
	std::mutex mtx; 
	TwoQueue tq;
	EvictableException eex;
};

#endif
