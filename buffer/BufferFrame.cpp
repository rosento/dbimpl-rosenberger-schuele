#include <stddef.h>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#include "BufferFrame.hpp"
#include "BufferManager.hpp"
#include "definitions.hpp"


BufferFrame::BufferFrame(uint64_t pNo)
{
	pageNo = pNo;
	dirty = false;
	latched = false;
	readingThreads=0;
	data = NULL;
}

BufferFrame::BufferFrame()
{
	dirty = false;
	latched = false;
	data = NULL;
	readingThreads=0;
}

void *BufferFrame::getData()
{
	mtx.lock();
	if(data==NULL){
		data = malloc(PAGE_SIZE);
		storage.readPage(pageNo,data);
	}
	mtx.unlock();
	return data;
	
}

bool BufferFrame::isLatched()
{
	return readingThreads > 0;
}

bool BufferFrame::isDirty()
{
	return dirty;

}
void BufferFrame::setDirty(bool newVal)
{
	mtx.lock();
	dirty=newVal || dirty;
	mtx.unlock();
}

void BufferFrame::lock(bool exclusive)
{
	mtx.lock();
	/*exklusives lock gesetzt ==> warte bis */
	/*exklusiver Zugriff gewollt ==> warte bis keiner mehr liest*/
	if(latched || exclusive){
		while(readingThreads>0){
			/*wait...*/	
			mtx.unlock();
			std::this_thread::sleep_for(std::chrono::seconds(1));
			mtx.lock();
		}
		if(exclusive){ //exklusiv ==> Sperre
			latched = true;
		}
	}
	readingThreads++;
	mtx.unlock();
}

void BufferFrame::unlock()
{
	mtx.lock();
	if(readingThreads>0)
		readingThreads--;
	latched = false; // latch is exclusiv, so it must be false now
	mtx.unlock();
}

void BufferFrame::setPageNo(uint64_t pNo)
{
	pageNo=pNo;
}

uint64_t BufferFrame::getPageNo()
{
	return pageNo;
}

BufferFrame::~BufferFrame()
{
	mtx.lock();
	/*write page mit PageId und dateninhalt*/
	if(dirty && data != NULL){
		storage.writePage(pageNo,data);
	}
	mtx.unlock();
	free(data);
}
