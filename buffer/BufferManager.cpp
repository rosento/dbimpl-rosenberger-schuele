#include <stddef.h>
#include <iostream>

#include "BufferManager.hpp"

BufferManager::BufferManager(unsigned pageCount)
{
	pagesInMem = pageCount;
}

BufferFrame& BufferManager::fixPage(uint64_t pageId, bool exclusive)
{
	mtx.lock();
	/*pruefe ob Seite geladen*/
	if(frames.count(pageId)<=0){ // Seite noch nicht drin => Einlagern
		
		if(frames.size()>=pagesInMem){ // Maximum an Seiten ==> Auslagern
			BufferFrame *bff = tq.remove();
			if(bff==NULL){
				mtx.unlock();
				throw eex;
			}
			std::map<uint64_t,BufferFrame*>::iterator it = frames.find(bff->getPageNo());
			frames.erase(it);
			delete bff;
		}

		frames[pageId]= new BufferFrame(pageId);
		tq.add(frames[pageId]); //der 2Q propagieren

	}else{ // Seite schon drin => zugreifen
		tq.again(frames[pageId]); // der 2Q propagieren
	}
	frames[pageId]->lock(exclusive);
	BufferFrame& mybf=*frames[pageId]; //wg der Rueckgabe
	mtx.unlock();
	return mybf;
}

void BufferManager::unfixPage(BufferFrame& frame, bool isDirty)
{
	/*setze frame dirty nur, wenn dirty oder vorher dirty :)*/
	frame.setDirty(isDirty);
	frame.unlock();
}

BufferManager::~BufferManager()
{
	// erwerbe lock fuer alle Seiten
	for (std::map<uint64_t,BufferFrame*>::iterator it=frames.begin();it!=frames.end(); it++){
		it->second->lock(true);
	}

	/*schreibe zurueck*/
	for (std::map<uint64_t,BufferFrame*>::iterator it=frames.begin();
		it!=frames.end(); it++){
		delete it->second;
	}//endfor
	// erase everything
	frames.erase(frames.begin(),frames.end());
}
