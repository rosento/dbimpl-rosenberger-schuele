#include <iostream>
#include "BTree.hpp"


template <class T, class CMP>
BTree<T,CMP>::BTree(uint64_t segment, BufferManager *bufferManager)
{
	bm = bufferManager;
	firstPage = segment << BITS_PAGE;
	count = 0;

	BufferFrame& bf = bm->fixPage(firstPage, true);

	struct Leaf<T,CMP> *myleaf = (struct Leaf<T,CMP> *) bf.getData();
	myleaf->lsn   = 0;
	myleaf->count = 0;
	myleaf->leaf  = true;
	myleaf->next  = 0;
	myleaf->prev  = 0;

	bm->unfixPage(bf,true);
	max = firstPage+1;
}

template <class T, class CMP>
BTree<T,CMP>::~BTree()
{

}

template <class T, class CMP>
void BTree<T,CMP>::split(uint64_t curPageID, uint64_t *rechts, T *key)
{
	//left, right and upper node, their pageID
	uint64_t leftPage = curPageID, rightPage = max++;
	
	BufferFrame& bf = bm->fixPage(curPageID, true);
	struct Node<T,CMP> *curNode = (struct Node<T,CMP> *) bf.getData();
	
	//create a second one
	BufferFrame& bfsecond = bm->fixPage(rightPage, true);
	if(curNode->leaf){ //create a new leaf
		struct Leaf<T,CMP> *oldLeaf = (struct Leaf<T,CMP> *) curNode; //cast to leaf
		//create the new leaf
		struct Leaf<T,CMP> *right = (struct Leaf<T,CMP> *) bfsecond.getData();
		right->lsn   = 0;
		right->leaf  = true;
		
		*key = oldLeaf->pairs[K_VALUE/2].key; // and set the key above

		//copy half
		for(unsigned i = 0; i < K_VALUE/2; i++){
			right->pairs[i].key = oldLeaf->pairs[i + K_VALUE/2].key;
			right->pairs[i].child = oldLeaf->pairs[i + K_VALUE/2].child;
		}
		right->count   = K_VALUE/2;
		oldLeaf->count = K_VALUE/2;
		// pointer to next
		uint64_t theNextPage = oldLeaf->next; //should be updated
		// <prev,node,next> --> <prev,node,toright>,<toleft,node,next>
		right->next  = oldLeaf->next;
		oldLeaf->next  = rightPage;
		right->prev = leftPage;

		bm->unfixPage(bfsecond,true);
		bm->unfixPage(bf,true);

		//notify the next leaf about the change
		BufferFrame& bfnext = bm->fixPage(theNextPage, true);
		struct Leaf<T,CMP> *theNextLeaf = (struct Leaf<T,CMP> *) bfnext.getData();
		theNextLeaf->prev = rightPage;
		bm->unfixPage(bfnext,true);

	}else{ //create a new InnerNode
		struct InnerNode<T,CMP> *oldNode = (struct InnerNode<T,CMP> *) curNode; //cast to InnerNode
		//create the new Node
		struct InnerNode<T,CMP> *right = (struct InnerNode<T,CMP> *) bfsecond.getData();
		right->lsn   = 0;
		right->leaf  = false;

		// childPage of the first of the second half is the new upper
		*key = oldNode->pairs[K_VALUE/2].key; // and set the key above
		
		//copy half except the first 
		for(unsigned i = 0; i+1 < K_VALUE/2; i++){
			right->pairs[i].key = oldNode->pairs[i+1 + K_VALUE/2 ].key;
			right->pairs[i].childPage = oldNode->pairs[i+1 + K_VALUE/2].childPage;
		}
		right->count   = K_VALUE/2 - 1;
		oldNode->count = K_VALUE/2;
		right->upper   = oldNode->pairs[K_VALUE/2].childPage;

		bm->unfixPage(bfsecond,true);
		bm->unfixPage(bf,true);
	}
	*rechts = rightPage;
}

template <class T, class CMP>
uint64_t BTree<T,CMP>::split(uint64_t curPageID)
{
	//left, right and upper node, their pageID
	uint64_t leftPage = curPageID, rightPage = 0, upperPage = max++;
	
	T key;
	split(curPageID, &rightPage, &key);

	//create the node above
	BufferFrame& bfi = bm->fixPage(upperPage, true);
	struct InnerNode<T,CMP> * above = (struct InnerNode<T,CMP> *) bfi.getData();
	above->lsn   = 0;
	above->count = 1;
	above->leaf  = false;
	above->upper = leftPage; //old is left
	above->pairs[0].childPage = rightPage;
	above->pairs[0].key = key;
	
	bm->unfixPage(bfi,true);
	return upperPage; //returs the id of the new upper page 
}


/** inserts a key and a given TID into a leaf
*/
template <class T, class CMP>
void BTree<T,CMP>::insertIntoLeaf(struct Leaf<T,CMP> *leaf, T key, TID tid)
{
	unsigned i = 0;
	while(i < leaf->count && smaller(leaf->pairs[i].key,key) )
		i++;
	//shift entries
	for(unsigned j = leaf->count; j>i ; j--){
		leaf->pairs[j].key = leaf->pairs[j-1].key;
		leaf->pairs[j].child = leaf->pairs[j-1].child;
	}
	//insert new one
	leaf->pairs[i].key = key;
	leaf->pairs[i].child = tid;
	leaf->count++;
}
/** inserts a key and a given TID into a innernode, calls itself until a leaf is reached
*/
template <class T, class CMP>
void BTree<T,CMP>::insertIntoIN(struct InnerNode<T,CMP> *in, T key, TID tid)
{
	//else: last one
	unsigned index = in->count - 1; //set index for update
	uint64_t pageID = in->pairs[index].childPage;
	bool callUpper = false;

	if(smaller(key,in->pairs[0].key)){
		pageID = in->upper;
		callUpper = true;
	}else{
		// if key < than first key
		for(unsigned i = 1; i < in->count; i++){
			// if key <= in->pairs[i].key <=> !(in->pairs[i].key < key)
			if(smaller(key, in->pairs[i].key)){
				// insert into the one before
				index = i-1;
				pageID = in->pairs[index].childPage;
				break;
			}
		}
	}
	
	// should it be split?
        BufferFrame& bf4Split = bm->fixPage(pageID, true);
        struct Node<T,CMP> *childNode = (struct Node<T,CMP> *) bf4Split.getData();
        // if full, split
        if(childNode->count == K_VALUE){
                bm->unfixPage(bf4Split,false);
                uint64_t rightPage;
                T retkey;
                split(pageID, &rightPage, &retkey);

                if(callUpper){//update, shift all right
                        for(unsigned i = in->count; i > 0; i--){
                                in->pairs[i].key = in->pairs[i-1].key;
                                in->pairs[i].childPage = in->pairs[i-1].childPage;
                        }
                        in->count++; //increment
                        in->pairs[0].key = retkey;
                        in->pairs[0].childPage = rightPage;
                }else{//update, shift right from index
                        for(unsigned i = in->count; i > index+1; i--){
                                in->pairs[i].key = in->pairs[i-1].key;
                                in->pairs[i].childPage = in->pairs[i-1].childPage;
                        }
                        in->count++; //increment
                        in->pairs[index+1].key = retkey;
                        in->pairs[index+1].childPage = rightPage;
                }
		//look whether this or the next node is looked
		if(!smaller(key,retkey))//so take the rightPage
			pageID = rightPage;	
        }else{
                bm->unfixPage(bf4Split,false);
        }

	
	// recursive call
	BufferFrame& bf = bm->fixPage(pageID, true);
	childNode = (struct Node<T,CMP> *) bf.getData();
	// if leaf, insert, unlock, done
	if(childNode->leaf){
		struct Leaf<T,CMP> *leaf = (struct Leaf<T,CMP> *) childNode; //cast to leaf
		insertIntoLeaf(leaf,key,tid);	
		bm->unfixPage(bf,true);
		return;
	}else{
		struct InnerNode<T,CMP> *in = (struct InnerNode<T,CMP> *) childNode; //cast to leaf
		insertIntoIN(in,key,tid);
		bm->unfixPage(bf,true);
	}
}

template <class T, class CMP>
void BTree<T,CMP>::insert(T key, TID tid)
{
	count++;//increment
	// lock root
	BufferFrame& bffirst = bm->fixPage(firstPage, true);
	struct Node<T,CMP> *curNode = (struct Node<T,CMP> *) bffirst.getData();
	// if full, split
	if(curNode->count == K_VALUE){
		bm->unfixPage(bffirst,false);
		firstPage = split(firstPage);
	}else{
		bm->unfixPage(bffirst,false);
	}
	// binary search
	BufferFrame& bf = bm->fixPage(firstPage, true);
	curNode = (struct Node<T,CMP> *) bf.getData();
	// if leaf, insert, unlock, done
	if(curNode->leaf){
		struct Leaf<T,CMP> *leaf = (struct Leaf<T,CMP> *) curNode; //cast to leaf
		insertIntoLeaf(leaf,key,tid);	
		bm->unfixPage(bf,true);
		return;
	}else{
		struct InnerNode<T,CMP> *in = (struct InnerNode<T,CMP> *) curNode; //cast to leaf
		insertIntoIN(in,key,tid);
		in->allcount = count; //save count for the next time :)
		bm->unfixPage(bf,true);
	}

	// lock below
	// go down
	// while (foo) {
	  // if full, split
	  // unlock above
	  // binary search
	  // if leaf, insert, unlock, done
	  // lock below
	  // go down
	// }
}

/**erases in Leaf
* returns -1: not found; else: number of elements
*/
template <class T, class CMP>
int BTree<T,CMP>::eraseInLeaf(struct Leaf<T,CMP> *leaf, T key)
{
	for(unsigned i = 0; i < leaf->count; i++){
		if(! (smaller(leaf->pairs[i].key, key) || smaller(key, leaf->pairs[i].key))  ){
			//shift all left
			for(unsigned j = i; j+1 < leaf->count; j++){
				leaf->pairs[j].key = leaf->pairs[j+1].key;
				leaf->pairs[j].child = leaf->pairs[j+1].child;
			}
			leaf->count = leaf->count > 0 ? leaf->count-1 : 0;
			if(leaf->count == 0){//prepare for delete, update pointers
				//notify the next leaf about the change
				if(leaf->next != 0){//exists next
					BufferFrame& bfnext = bm->fixPage(leaf->next, true);
					struct Leaf<T,CMP> *theNextLeaf = (struct Leaf<T,CMP> *) bfnext.getData();
					theNextLeaf->prev = leaf->prev;
					bm->unfixPage(bfnext,true);
				}
				if(leaf->prev != 0){
					BufferFrame& bfprev = bm->fixPage(leaf->prev, true);
					struct Leaf<T,CMP> *thePrevLeaf = (struct Leaf<T,CMP> *) bfprev.getData();
					thePrevLeaf->next = leaf->next;
					bm->unfixPage(bfprev,true);
				}
			}
			return leaf->count;
		}
	}
	return -1;
}
/** return 0 on error, 1 on success, or the new PageID
*/
template <class T, class CMP>
uint64_t BTree<T,CMP>::eraseInIN(struct InnerNode<T,CMP> *in, T key)
{
	//else: last one
	unsigned index = in->count > 0 ? in->count - 1 : 0; //set index for update
	uint64_t pageID = in->pairs[index].childPage;
	bool callUpper = false;

	if(smaller(key,in->pairs[0].key)){
		pageID = in->upper;
		callUpper = true;
	}else{
		// if key < than first key
		for(unsigned i = 1; i < in->count; i++){
			// if key <= in->pairs[i].key <=> !(in->pairs[i].key < key)
			if(smaller(key, in->pairs[i].key)){
				// the one before
				index = i-1;
				pageID = in->pairs[index].childPage;
				break;
			}
		}
	}
	
	// recursive call
	BufferFrame& bf = bm->fixPage(pageID, true);
	struct Node<T,CMP> *childNode = (struct Node<T,CMP> *) bf.getData();
	// if leaf, delete, unlock, done
	if(childNode->leaf){
		struct Leaf<T,CMP> *leaf = (struct Leaf<T,CMP> *) childNode; //cast to leaf
		int ret = eraseInLeaf(leaf,key);
		if(ret == -1){//not found
			bm->unfixPage(bf,true);
			return 0;
		}else if(ret == 0){//union
			if(callUpper){//shift left all
				in->upper = in->pairs[0].childPage;
				for(unsigned i = 0; i+1 < in->count; i++){
					in->pairs[i].key = in->pairs[i+1].key;
					in->pairs[i].childPage = in->pairs[i+1].childPage;
				}
			}else{
				for(unsigned i = index; i+1 < in->count; i++){
					in->pairs[i].key = in->pairs[i+1].key;
					in->pairs[i].childPage = in->pairs[i+1].childPage;
				}
			
			}
			in->count = in->count > 0 ? in->count-1 : 0;//decrement
			if(in->count == 0){
				pageID = in->upper;
				bm->unfixPage(bf,true);
				return pageID;
			}
		}
	}else{
		struct InnerNode<T,CMP> *in = (struct InnerNode<T,CMP> *) childNode; //cast to leaf
		uint64_t ret = eraseInIN(in,key);
		if(ret==0){//error
			bm->unfixPage(bf,true);
			return 0;
		}else if(ret>1){//new PageID
			if(callUpper){
				in->upper = ret;
			}else{
				in->pairs[index].childPage = ret;
			}
		}
	}
	bm->unfixPage(bf,true);
	return 1;
}

template <class T, class CMP>
void BTree<T,CMP>::erase(T key)
{
	BufferFrame& bf = bm->fixPage(firstPage, true);
	struct Node<T,CMP> *curNode = (struct Node<T,CMP> *) bf.getData();
	// if leaf, delete, unlock, done
	if(curNode->leaf){
		struct Leaf<T,CMP> *leaf = (struct Leaf<T,CMP> *) curNode; //cast to leaf
		int ret = eraseInLeaf(leaf,key);
		bm->unfixPage(bf,true);
		if(ret>=0){//was found
			count = ret;//decrement
		}
	}else{
		struct InnerNode<T,CMP> *in = (struct InnerNode<T,CMP> *) curNode; //cast to leaf
		uint64_t ret = eraseInIN(in,key);
		if(ret==0){//Fehler
			bm->unfixPage(bf,true);
			return;
		}else if(ret>1){//new PageID
			firstPage = ret;
		}
		count = count > 0 ? count - 1 : 0 ;//decrement
		in->allcount = count; //save count for the next time :)
		bm->unfixPage(bf,true);
	}
}
/** tail-recursive class of lookup
*/
template <class T, class CMP>
bool BTree<T,CMP>::lookup(T key, TID& tid, uint64_t pageID)
{
	BufferFrame& bf = bm->fixPage(pageID, true);
	struct Node<T,CMP> *curNode = (struct Node<T,CMP> *) bf.getData();
	
	if(curNode->leaf){//is a Leaf
		struct Leaf<T,CMP> *leaf = (struct Leaf<T,CMP> *) curNode; //cast to leaf

		for(unsigned i = 0; i < leaf->count; i++){
			// a < b == false && b < a == false ==> a=b
			// a=b <=> !(a<b || b>a)
			if(! (smaller(leaf->pairs[i].key, key) || smaller(key, leaf->pairs[i].key))  ){
				tid = leaf->pairs[i].child;
				bm->unfixPage(bf,false);
				return true;
			}
		}
		bm->unfixPage(bf,false);
	}else{//is a InnerNode
		struct InnerNode<T,CMP> *in = (struct InnerNode<T,CMP> *) curNode; //cast to InnerNode
		uint64_t childPage = in->pairs[in->count-1].childPage;
		if(smaller(key,in->pairs[0].key)){
			childPage = in->upper;
		}else{
			for(unsigned i = 1; i < in->count; i++){
				if(smaller(key, in->pairs[i].key)){
					childPage = in->pairs[i-1].childPage;
					break; // when the value is found
				}
			}
		}
		bm->unfixPage(bf,false);
		return lookup(key,tid,childPage);
	}

	return false;
}

template <class T, class CMP>
bool BTree<T,CMP>::lookup(T key, TID& tid)
{
	return lookup(key,tid,firstPage);
}


template <class T, class CMP>
std::iterator<std::input_iterator_tag,T> BTree<T,CMP>::lookupRange()
{

}

template <class T, class CMP>
uint64_t BTree<T,CMP>::size()
{
	return count;
}


template <class T, class CMP>
void BTree<T,CMP>::print(uint64_t pageID)
{
	BufferFrame& bf = bm->fixPage(pageID, true);
	struct Node<T,CMP> *curNode = (struct Node<T,CMP> *) bf.getData();
	
	if(curNode->leaf){//is a Leaf
		struct Leaf<T,CMP> *leaf = (struct Leaf<T,CMP> *) curNode; //cast to leaf
		std::cout << "Leaf #" << pageID << " " << leaf->count << ": ";
		for(unsigned i = 0; i < leaf->count; i++){
			std::cout << leaf->pairs[i].key << "; ";
		}
		std::cout << "prev: " << leaf->prev << "; next: " << leaf->next <<std::endl;
		bm->unfixPage(bf,false);
	}else{//is a InnerNode
		struct InnerNode<T,CMP> *in = (struct InnerNode<T,CMP> *) curNode; //cast to InnerNode
		
		std::cout << "InnerNode #" << pageID << " " << in->count << ": ";
		for(unsigned i = 0; i < in->count; i++){
			std::cout << in->pairs[i].key << "; ";
		}
		std::cout << std::endl << "Kinder: " << std::endl;
		print(in->upper);
		for(unsigned i = 0; i < in->count; i++){
			print(in->pairs[i].childPage);
		}
		bm->unfixPage(bf,false);
	}
}
template <class T, class CMP>
void BTree<T,CMP>::print()
{
	print(firstPage);
}
