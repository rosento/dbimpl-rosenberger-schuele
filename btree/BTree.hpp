#ifndef BTREE_HPP
#define BTREE_HPP

#include <cstdint>
#include <cstdlib>
#include <iterator>

#include "../buffer/definitions.hpp"
#include "../slottedpages/TID.hpp"
#include "../buffer/BufferManager.hpp"

//#define TID uint64_t

#define K_VALUE 4

template <class T, class CMP>
struct Node{
	uint64_t lsn;
	unsigned count;
	bool leaf;
};

template <class T, class CMP>
struct InnerNode : Node<T,CMP>{
	uint64_t upper;
	unsigned allcount;
	struct Pairs{
		T key;
		uint64_t childPage; //pointer to child pagenumber
	} pairs[K_VALUE];

};

template <class T, class CMP>
struct Leaf : Node<T,CMP>{
	uint64_t next, prev;
	struct Elems{
		T key;
		TID child;
	} pairs[K_VALUE];
};

template <class T, class CMP>
class BTree{
	public:
		BTree(uint64_t segment, BufferManager *bufferManager);
		~BTree();
		uint64_t split(uint64_t);
		void split(uint64_t curPageID, uint64_t *rechts, T *key);
		void insertIntoLeaf(struct Leaf<T,CMP> *leaf, T key, TID tid);
		void insertIntoIN(struct InnerNode<T,CMP> *in, T key, TID tid);
		void insert(T key, TID tid);
		void erase(T key);
		bool lookup(T key, TID& tid, uint64_t pageID);
		bool lookup(T key, TID& tid);
		std::iterator<std::input_iterator_tag, T> lookupRange();
		uint64_t size();
		void print();
		void print(uint64_t pageID);

	private:
		int eraseInLeaf(struct Leaf<T,CMP> *leaf, T key);
		uint64_t eraseInIN(struct InnerNode<T,CMP> *in, T key);

		uint64_t firstPage;
		BufferManager *bm;
		unsigned max;
		CMP smaller;
		unsigned count;
};

#endif
