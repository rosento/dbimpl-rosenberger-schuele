#ifndef HASHJOINOP_HPP
#define HASHJOINOP_HPP

#include <vector>
#include <iostream>
#include <sstream>
#include <unordered_map>

#include "Operator.hpp"

class HashJoinOP : public Operator{
	public:
		HashJoinOP(Operator *left, Operator *right, unsigned regLeft, unsigned regRight);
		~HashJoinOP();
		void open();
		bool next();
		std::vector<Register *> getOutput();
		void close();
	private:
		Operator *left, *right;
		unsigned regLeft, regRight;
		std::vector<Register *> regs;
		std::unordered_multimap<Register, std::vector<Register *> > hashTable;
		//iterator for the elements
		std::pair < std::unordered_multimap<Register,std::vector<Register *>>::iterator,
			std::unordered_multimap<Register,std::vector<Register *>>::iterator
			> equalit;
		bool somerowsleft;

};
#endif
