#ifndef PRINTOP_HPP
#define PRINTOP_HPP

#include <vector>
#include <iostream>
#include <sstream>

#include "Operator.hpp"

/*makes a relation with the given value*/
class DummyOP : public Operator{
	public:
		DummyOP(int,int);
		~DummyOP();
		void open();
		bool next();
		std::vector<Register*> getOutput();
		void close();
	private:
		int value, count;
};
#endif
