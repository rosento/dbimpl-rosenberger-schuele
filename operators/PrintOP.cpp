#include "PrintOP.hpp"

PrintOP::PrintOP(std::stringstream *_out, Operator *_op)
{
	out = _out;
	op = _op;
}

PrintOP::~PrintOP()
{
	op->close();
}

void PrintOP::open()
{
	op->open();
}

bool PrintOP::next()
{
	bool result = op->next();
	if (result) {
		for (Register *reg : op->getOutput()) {
				switch (reg->getKind()) {
					case INT:
						(*out) << reg->getInteger() << '\t';
						break;
					case STRING:
						(*out) << reg->getString() << '\t';
						break;
					default:
						(*out) << "invalid type\t";
				}
		}
		(*out) << '\n';
	}
	return result;
}

std::vector<Register*> PrintOP::getOutput()
{
return op->getOutput();
}

void PrintOP::close()
{
	op->close();
}
