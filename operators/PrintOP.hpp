#ifndef PRINTOP_HPP
#define PRINTOP_HPP

#include <vector>
#include <iostream>
#include <sstream>

#include "Operator.hpp"

class PrintOP : public Operator{
	public:
		PrintOP(std::stringstream *, Operator *);
		~PrintOP();
		void open();
		bool next();
		std::vector<Register*> getOutput();
		void close();
	private:
		std::stringstream *out;
		Operator *op;
};
#endif
