#ifndef REGISTER_HPP
#define REGISTER_HPP

#include <string>
#include <cstdint>

enum Kind {DEF, STRING, INT}; //about the kind of this register

class Register{
	public:
		Register();
		Register(int);
		Register(std::string);
		void setInteger(const int);
		void setString(const std::string &);
		int getInteger();
		Kind getKind();
		std::string getString();
		bool operator==(Register) const;
		bool operator<(Register) const;
		size_t hash() const;

	private:
		int intvalue;
		std::string stringvalue;
		Kind kind;
};

namespace std {
	template <>
	struct hash<Register>
	{
		std::size_t operator()(const Register& k) const
		{
			return k.hash();
		}
	};
}

#endif
