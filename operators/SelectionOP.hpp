#ifndef SELECTIONOP_HPP
#define SELECTIONOP_HPP

#include <vector>
#include <iostream>
#include <sstream>

#include "Operator.hpp"

class SelectionOP : public Operator{
	public:
		SelectionOP(Operator *op, unsigned registerID, Register constant);
		~SelectionOP();
		void open();
		bool next();
		std::vector<Register*> getOutput();
		void close();
	private:
		std::vector<Register*> regs;
		Register constant;
		unsigned row;
		Operator *op;
};
#endif
