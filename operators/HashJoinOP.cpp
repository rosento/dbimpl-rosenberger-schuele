#include "HashJoinOP.hpp"

HashJoinOP::HashJoinOP(Operator *left, Operator *right, unsigned regLeft, unsigned regRight)
{
	this->left 	= left;
	this->right 	= right;
	this->regLeft 	= regLeft;
	this->regRight 	= regRight;
	somerowsleft 	= false;
}

HashJoinOP::~HashJoinOP()
{

}

void HashJoinOP::open()
{
	//read all from the left
	left->open();
	while(left->next()){
		std::vector<Register *> leftValues = left->getOutput();
		//take the regLeft^th value as join value
		hashTable.emplace(*leftValues[regLeft], leftValues);
	}

	right->open();
}

bool HashJoinOP::next()
{
	if(somerowsleft){
		std::vector<Register *> rightValues = right->getOutput();
		regs = equalit.first->second; //copy the left reg vals
		//add the left ones
		for(unsigned i=0; i<rightValues.size(); i++)
			regs.push_back(rightValues[i]);

		//for next step: some values left?
		if(++equalit.first == equalit.second)
			somerowsleft = false;
		else
			somerowsleft = true;
		return true;
	}

	while(right->next()){
		std::vector<Register *> rightValues = right->getOutput();
		//lookup for the value of the regRight^th register
		equalit = hashTable.equal_range(*rightValues[regRight]);
		//nothing found?
		if(equalit.first == equalit.second)
			continue;
		regs = equalit.first->second; //copy the left reg vals
		//add the left ones
		for(unsigned i=0; i<rightValues.size(); i++)
			regs.push_back(rightValues[i]);
		//for next step: some values left?
		if(++equalit.first == equalit.second)
			somerowsleft = false;
		else
			somerowsleft = true;
		return true;
	}
	return false;
}

std::vector<Register*> HashJoinOP::getOutput()
{
	return regs;
}

void HashJoinOP::close()
{
	left->close();
	right->close();
}
