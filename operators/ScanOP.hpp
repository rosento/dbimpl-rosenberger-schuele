#ifndef SCAN_HPP
#define SCAN_HPP

#include <vector>
#include <iostream>
#include <sstream>

#include "Operator.hpp"
#include "../slottedpages/SPSegment.hpp"
#include "../schema/Schema.hpp"

/*makes a relation with the given value*/
class ScanOP : public Operator{
	public:
		ScanOP(SPSegment *segment, Schema::Relation *rel);
		~ScanOP();
		void open();
		bool next();
		std::vector<Register*> getOutput();
		void close();
	private:
		std::vector<Register *> registers;
		int constLength; // the length of the constant size entries
		SPSegment *segment;
		Schema::Relation *rel;
		uint64_t tid = 0;
};
#endif
