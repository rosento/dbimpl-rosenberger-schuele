#include "DummyOP.hpp"

DummyOP::DummyOP(int value, int count)
{
	this->value = value;
	this->count = count;
}

DummyOP::~DummyOP()
{

}

void DummyOP::open()
{

}

bool DummyOP::next()
{
	return count-- > 0;
}

std::vector<Register*> DummyOP::getOutput()
{
	std::vector<Register *> ret;
	for(int i=0; i<5; i++){
		ret.push_back(new Register(i));
	}
	return ret;
}

void DummyOP::close()
{
	//nothing
}
