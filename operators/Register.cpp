
#include "Register.hpp"

Register::Register()
{
	kind = DEF;
}

Register::Register(int i)
{
	setInteger(i);
}

Register::Register(std::string s)
{
	setString(s);
}

void Register::setInteger(const int i)
{
	intvalue = i;
	kind = INT;
}

void Register::setString(const std::string & s)
{
	kind = STRING;
	stringvalue = s;
}

int Register::getInteger()
{
	return intvalue;
}

std::string Register::getString()
{
	return stringvalue;
}

Kind Register::getKind()
{
	return kind;
}

size_t Register::hash() const {
	std::hash<std::string> stringHash;
	std::hash<int> intHash;
	switch(kind) {
		case INT:
			return intHash(intvalue);
		case STRING:
			return stringHash(stringvalue);
		default:
			return 0;
	}
}

bool Register::operator==(Register sec) const
{
	switch(kind){
		case INT:
			if(sec.kind == INT)
				return sec.getInteger() == intvalue;
			return false;
		case STRING:
			if(sec.kind == STRING)
				return sec.getString() == stringvalue;
			return false;
		default:
			return false;
	}
}

bool Register::operator<(Register sec) const
{
	switch(kind){
		case INT:
			if(sec.kind == INT)
				return intvalue < sec.getInteger();
			return false;
		case STRING:
			if(sec.kind == STRING)
				return stringvalue < sec.getString();
			return false;
		default:
			return false;
	}

}
