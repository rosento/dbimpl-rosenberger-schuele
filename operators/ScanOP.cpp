#include <string>

#include "ScanOP.hpp"

//using Schema::Relation;
//using Types;

ScanOP::ScanOP(SPSegment *_segment, Schema::Relation *_rel)
{
	segment = _segment;
	rel = _rel;
}

ScanOP::~ScanOP()
{
	delete segment;
}

void ScanOP::open()
{
	constLength = 0;
	tid = 0;

	// for attrs
	for (Schema::Relation::Attribute attr : rel->attributes) { //get attrs
	//  create registers
		switch (attr.type) {
			case Types::Tag::Integer:
				registers.push_back(new Register(0));
				break;
			case Types::Tag::Char:
				registers.push_back(new Register(""));
				break;
		}
	//  sum constLength
		constLength += sizeof(int);
	}
}

bool ScanOP::next()
{
	uint64_t max = segment->getMaxTID(); //TODO get maximum TID
	for (; tid <= max; tid++) {
		Record rec = segment->lookup(tid);
		const char *data = rec.getData();
		int *constData = (int *) data; // constant length data
			
		if (data) {
			int constPos = 0;
			int varPos = constLength;
			for (Register *reg : registers) {
				switch (reg->getKind()) {
					case INT:
						reg->setInteger(constData[constPos]);
						constPos++;
						break;
					case STRING:
						reg->setString(*(new std::string(data + varPos)));
						varPos = constData[constPos];
						constPos++;
						break;
					default:
						// some clown changed the available types
						exit(1);
				}
			}
			tid++;
			return true;
		}
	}
	return false;
}

std::vector<Register*> ScanOP::getOutput()
{
	return registers;
}

void ScanOP::close()
{
	delete segment;
}
