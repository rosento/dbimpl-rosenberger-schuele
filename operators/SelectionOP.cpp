#include "SelectionOP.hpp"

SelectionOP::SelectionOP(Operator *op, unsigned registerID, Register constant)
{
	this->row = registerID;
	this->constant = constant;
	this->op = op;
}

SelectionOP::~SelectionOP()
{
}

void SelectionOP::open()
{
	op->open();
}

bool SelectionOP::next()
{
	while(op->next()){
		regs = op->getOutput();
		if(*regs[row] == constant)
			return true;
	}
	return false;
}

std::vector<Register*> SelectionOP::getOutput()
{
	return regs;
}

void SelectionOP::close()
{
	op->close();
}
