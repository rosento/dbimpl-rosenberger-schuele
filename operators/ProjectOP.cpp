#include "ProjectOP.hpp"

ProjectOP::ProjectOP(Operator *_op, std::list<int> _indxs)
{
	op = _op;
	indxs = _indxs;
}

ProjectOP::~ProjectOP()
{
	op->close();
}

void ProjectOP::open()
{
	op->open();
}

bool ProjectOP::next()
{
	return op->next();
}

std::vector<Register*> ProjectOP::getOutput()
{
	std::vector<Register *> ret;
	const std::vector<Register *> &orig = op->getOutput();
	for (int i : indxs) {
		ret.push_back(orig.at(i));
	}
	return ret;
}

void ProjectOP::close()
{
	op->close();
}
