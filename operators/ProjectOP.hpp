#ifndef PRINTOP_HPP
#define PRINTOP_HPP

//TODO remove unnecessary stuff

#include <vector>
#include <iostream>
#include <sstream>
#include <list>

#include "Operator.hpp"

/*makes a relation with the given value*/
class ProjectOP : public Operator{
	public:
		ProjectOP(Operator *, std::list<int>);
		~ProjectOP();
		void open();
		bool next();
		std::vector<Register*> getOutput();
		void close();
	private:
		int value, count;
		Operator *op;
		std::list<int> indxs;
};
#endif
