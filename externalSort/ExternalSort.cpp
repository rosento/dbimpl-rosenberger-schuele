#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>

#include <stdint.h>

#include <unistd.h>
#include <fcntl.h>

struct run {
	uint64_t value;
	int fd;
	uint64_t run_start;
	uint64_t run_size;
	uint64_t chunk_size;
	uint64_t chunk_num;
	uint64_t pos;
	uint64_t eSize;
	uint64_t* buf;
	ssize_t io(int fd, void *buf, size_t chunk_size, off_t offset) {
		return 0;
	}
	void advance() {
		pos++;
		if (pos * sizeof(uint64_t) >= chunk_size) {
			pos = 0;
			chunk_num++;
			io(fd, buf, chunk_size, run_start + chunk_num * chunk_size);
		}
		value = buf[pos];
	}
	bool operator>(run const other) const {
		return value > other.value;
	}
};

struct run_in : run {
	ssize_t io(int fd, void *buf, size_t chunk_size, off_t off){
		return pread(fd, buf, chunk_size, off);
	}
	bool empty() {
		uint64_t absPos = chunk_num*chunk_size + pos*sizeof(int64_t);
		return absPos >= run_size;
	}
};

struct run_out : run {
	ssize_t io(int fd, void *buf, size_t chunk_size, off_t off){
		return pwrite(fd, buf, chunk_size, off);
	}
	void set(uint64_t newVal) {
		value = newVal;
		buf[pos] = value;
	}
};

void merge(uint64_t k, int fd, off_t off1, off_t off2, uint64_t runSize, uint64_t chunk_size, uint64_t eSize, uint64_t size) {
	std::priority_queue<run_in, std::vector<run_in>, std::greater<run_in> > queue;
	run_in current;
	run_out result;
	result.fd = fd;
	result.run_start = off2;
	result.run_size = k * runSize;
	result.chunk_size = chunk_size;
	result.chunk_num = 0;
	result.pos = 0;
	result.eSize = eSize;
	result.buf = (uint64_t *) malloc(chunk_size);
	
	// load first chunk of each run, put first elem into queue
	for (uint64_t i = 0; i < k; i++) {
		uint64_t *buf =  (uint64_t *) malloc(chunk_size);
		pread(fd, buf, chunk_size, off1 + i*runSize);
		current.value = buf[0];
		current.fd = fd;
		current.run_start = off1 + i*runSize;
		current.run_size = runSize;//i + 1 < k ? runSize : size - i*runSize;
		std::cerr << "run size: " << current.run_size << std::endl;
		current.chunk_size = chunk_size;
		current.chunk_num = 0;
		current.pos = 0;
		current.buf = buf;
		queue.push(current);
	}
	
	// merge the selected runs while there is something to merge
	for(int i=0; i<10000000 && ! queue.empty(); i++) {
		current = queue.top();
		queue.pop();
		result.set(current.value);
		if (! queue.empty()) {
			result.advance();
		}
		if (current.empty()) {
			free(current.buf);
		} else {
			current.advance();
			queue.push(current);
		}
	}
	
	pwrite(fd, result.buf, chunk_size, result.run_start + result.chunk_num * chunk_size);

	free(result.buf);
}

void externalSort(int fdInput, uint64_t size, int fdOutput, uint64_t memSize) {
	size = size * sizeof(uint64_t);
	uint64_t eSize = memSize; // effective memory size; TODO perhaps less
	uint64_t chunk_size = getpagesize();
	uint64_t k = eSize / chunk_size;
	if(k<2) {
		k=2;
	}

	eSize = k * chunk_size;

	// allocate disk space for sorted/intermediate data
	int falloc = posix_fallocate(fdOutput, 0, 2 * size);
	if (falloc) {
		std::cerr << "Disk space allocation failed.\n Error number:" << falloc << "\nPlease refer to posix_fallocate(3)." << std::endl;
		exit(falloc);
	}

	// predict number of merges
	uint64_t mergeCount = 0; // = log(k, eSize);
	for (uint64_t runSize = eSize; runSize <= size && k > 1; runSize *= k) {
		mergeCount++;
	}
	
	std::cerr << "mergeCount: " << mergeCount << "; k: " << k<< std::endl;
	// diskspace=2 * original size
	// even times of merging ==> use first half to store data
	// ode times of merging ==> use second half to store data
	// ensure results end up in the beginning of file
	uint64_t off1 = 0, off2 = size, off3;
	if (mergeCount % 2 != 0) {
		off1 = off2;
		off2 = 0;
	}
	
	// sort each of the initial runs
	uint64_t *run = (uint64_t *) malloc(eSize);
	for (uint64_t i = 0; i * eSize < size; i++) {
		// last run: this_size will be less
		uint64_t this_size = (i+1) * eSize < size ? eSize : size- i*eSize;

		int bytes = pread(fdInput, run, this_size, off1 + i*eSize);
		std::cerr << "read " << bytes << " bytes" << std::endl;
		
		std::sort(run, run + this_size/sizeof(uint64_t));

		// store run
		pwrite(fdOutput, run, this_size, off1 + i*eSize);
	}
	free(run);

	// merge runs
	for (uint64_t runSize = eSize; runSize <= size && k > 1; runSize *= k) {
		for (uint64_t i = 0; i * runSize <= size; i+=k) {
			merge(k, fdOutput, off1 + i*runSize, off2 + i*runSize, runSize, chunk_size, eSize, size);
		}

		// swap off1 and off2
		off3 = off1;
		off1 = off2;
		off2 = off3;
	}

	ftruncate(fdOutput, size);
}
