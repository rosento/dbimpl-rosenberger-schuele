Dies sind die DBImpl-Lösungen von Tobias Rosenberger und Maximilian E. Schüle.

#Bau-, Test- und Ausführanleitung
1. gehe ins Verzeichnis gtest-1.7.0, fuehre ./configure aus
2. dann zurueck cd..
3. make all kompiliert alle Dateien
4. in bin/ finden sich die Programme und Tests

#Verzeichnisstruktur
* /bin: Binaerdateien, nicht im git
* /gtest-1.7.0: Dateien fuer googleTest
* /externalSort: zum ersten UE
* /buffer: zum zweiten UE
* /testing: die Tests dazu 

#Getestete Plattformen/Konfigurationen
* gcc-Version 4.7.3
* in externalSort: often the order is violated after 1024 numbers, but we couldn't find out the reason yet

#BufferManager
* Die 2Q-Implementierung hat
  - eine Methode zum hinzufügen eines noch nicht geladenen Frames
  - eine Methode zum entfernen aus der FIFO/LRU-Queue und neuen einfügen in die LRU-Queue
  - eine Methode zum Entfernen eines Frames, das durch die 2Q ausgewählt wird.
* Die 2Q benutzt
  - 2 Listen für die beiden Queues
  - 2 "std::unordered\_map"s mit Iteratoren für die Listenelemente, um konstante Zugriffszeit vor allem beim Entfernen aus den Queues zu haben.
* zum Interface der 2Q siehe buffer/TwoQueue.hpp, zur Implementierung buffer/TwoQueue.cpp
* Zum Sperren existieren zwei verschiedene Locks, ein globales in BufferManager und ein lokales pro Seite in der jeweiligen BufferFrame
  - BufferManager::fixPage(): wird bewacht durch das globale Lock, damit nicht zeitgleich zwei Threads eine neue Seite anfordern und sie einlagern lassen; anschließend wird die Seite mit dem jeweiligen Lock (exclusive=true oder shared=false) versehen und das globale Lock wird freigegeben
  - BufferManager::~BufferManager(): Bevor die Seiten geschrieben werden, wird eine exklusive Sperre auf die Seiten angefordert mit lock(true)
  - BufferFrame: kritische Methoden sind lock(), unlock(), setDirty(), geschützt durch ein Mutex
  - BufferFrame::lock(): wenn exklusiv angefordert oder gesperrt ist, dann wartet der Thread, anschließend wird die Zahl der lesenden Threds inkrementiert
  - BufferFrame::unlock(): dekrementiert die Zahl der lesenden Threads und setzt exklusiv auf false

#Schema storage
(cd buffer; make)
(cd slottedpages; make)
(cd schema; make)

schema/parser file.sql

sollte ein Schema aus einer SQL-Datei lesen, in der Datenbank speichern, wieder auslesen und ausgeben.

#BTree - rangeLookup
Beim BTree haben wir eine Iterator und rangeLookup-Implementierung in commit 9f5ffe6540eef24.
Da der Iterator das BufferFrame kennen muss, und bei mutex der Zuweisungsoperator gelöscht ist,
mussten wir dort den Mutex aus dem BufferFrame ausbauen, weil sonst manche Zuweisungen nicht funktionieren würden.
In dem Commit mit rangeLookup terminieren allerdings die Tests nicht; wir vermuten, dass das mit den ausgebauten Mutexen zusammenhängt.
