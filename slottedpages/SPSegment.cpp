

#include "SPSegment.hpp"
#include <iostream>

SPSegment::SPSegment(uint64_t mySegment)
{
	bm = new BufferManager(10);
	init(mySegment);
}

SPSegment::SPSegment(uint64_t mySegment, BufferManager *bma)
{
	bm = bma;
	init(mySegment);
}
// inits an Object of a certain segment on which will be accessed
void SPSegment::init(uint64_t mySegment)
{
	//first page as freeSpace
	firstPage = mySegment << BITS_PAGE;
	for(unsigned i=0; i<4; i++){
		BufferFrame& firstFrame = bm->fixPage(firstPage + i, true);
		freeSpaceInventory[i] = (uint16_t *) firstFrame.getData();
	}
	// geg.: 2^64 Adressen, 1 Seite 4096 B groß
	// ==> 16bit fur Belegung, also 2B
	// 2048 Seiten pro Seite von freeSpace verwaltet
	// pro Segment: 20 Bits fur Seite, also 2^20 also 
	// anzahl SeitenfreeSpace=BITS_PAGE/16, MAX 4

	//has to be intiated?
	if(freeSpaceInventory[0][0] == 0){
		reset();
		freeSpaceInventory[0][1]=1; //first page is set
	}
}

SPSegment::~SPSegment()
{

}

TID SPSegment::insert(const Record& r)
{
	int pageID;
	SlottedPage *p;
	//lookup for first page with enough space
	//begin with the first not freeSpaceInventory page
	for(pageID=4; pageID<PAGES && getFreeSpaceOf(pageID) < r.getLen(); pageID++){
		//is not enough ==> continue
		if(getFreeSpaceOf(pageID) < r.getLen())
			continue;
		//take it :)
		break;
	}
	
	//last page ==> null	
	if(pageID==PAGES)
		return TID(0x0);

	BufferFrame& fixedFrame = bm->fixPage(firstPage + pageID,true);
	p = new SlottedPage(fixedFrame.getData());

	// not inititated ==> reset
	if(getFreeSpaceOf(pageID)==PAGE_SIZE)
		p->reset();

//	std::cout << "SPSegment.PageID: " << pageID "; freeInventory:" << getFreeSpaceOf(pageID) << "; to insert len: " << r.getLen() << std::endl;
		
//	std::cout << "freeInPage:" << p->getFreeSpace() << std::endl;
	setFreeSpaceOf(pageID, p->getFreeSpace());

	uint64_t index = p->insert(r.getLen(),r.getData());

//	std::cout << "freeInPage:" << p->getFreeSpace() << std::endl;	
	//update freeSpaceInventory
	setFreeSpaceOf(pageID, p->getFreeSpace());

	bm->unfixPage(fixedFrame,true);
	delete p;

//	std::cout << "index:" << index << std::endl;
	//return a TID with the given index and a certain page number
	TID result(pageID, index);
	maxTID = std::max(maxTID, result.getValue());
	return result;
}

bool SPSegment::remove(TID tid)
{
	//fix the wanted page
	BufferFrame& fixedFrame = bm->fixPage(firstPage+tid.getPageNo(),false);
	SlottedPage p(fixedFrame.getData());
	//and let the page do the work
	bool b = p.remove(tid.getIndex());
	setFreeSpaceOf(tid.getPageNo(), p.getFreeSpace());
	bm->unfixPage(fixedFrame,false);
	return b;
}

Record SPSegment::lookup(TID tid)
{
	//fix the wanted page
	BufferFrame& fixedFrame = bm->fixPage(firstPage+tid.getPageNo(),false);
	SlottedPage p(fixedFrame.getData());
	//and let the page do the work
	Record r = p.lookup(tid.getIndex());
	bm->unfixPage(fixedFrame,false);
	return std::move(r);
}

bool SPSegment::update(TID tid, const Record& r)
{
	BufferFrame& fixedFrame = bm->fixPage(firstPage+tid.getPageNo(),true);
	SlottedPage p(fixedFrame.getData());
	bool b = p.update(tid.getIndex(), r.getLen(), r.getData());
	setFreeSpaceOf(tid.getPageNo(), p.getFreeSpace());
	bm->unfixPage(fixedFrame,true);
	return b;
}

//FreeSpaceInventory is stored in the first pages of the current segment
unsigned SPSegment::getFreeSpaceOf(uint64_t pageID)
{
	if(pageID > 3) //ignore the freeSpaceInventory
		return freeSpaceInventory[pageID/2048][pageID];
	else
		return 0;
}

void SPSegment::setFreeSpaceOf(uint64_t pageID, uint16_t value)
{
	if(pageID > 3)
		freeSpaceInventory[pageID/2048][pageID] = value;

}

void SPSegment::reset()
{
	for(int i = 0; i < 2048; i++){
		setFreeSpaceOf(i,PAGE_SIZE);
	}
}

uint64_t SPSegment::getMaxTID()
{
	return maxTID;
}
