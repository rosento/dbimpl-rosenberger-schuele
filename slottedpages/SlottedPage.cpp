#include <iostream>

#include "SlottedPage.hpp"

SlottedPage::SlottedPage(void* page)
{
	header = (struct SPHeader *) page; //struct auf page hart drauf
	pagebegin = (char *) page; //to save begin pos
	slots = (struct Slot *) (pagebegin + sizeof(struct SPHeader)); //growing slots;
	
	//has to be initiated?
	if(header->dataStart == 0){
		reset();
	}
}

SlottedPage::~SlottedPage()
{

}

unsigned SlottedPage::getFreeSpace()
{
	//space between last slot and last data
	//pagebegin + header + all slots < first data block (or new)
	unsigned i = header->firstFreeSpace - ( sizeof(struct SPHeader) + header->slotCount * sizeof(struct Slot));
	
	if(i > sizeof(struct SPHeader))
		return i - sizeof(struct SPHeader);
	else
		return 0;
}

bool SlottedPage::isEnoughSpace(unsigned len)
{
	//space between last slot and last data
	//pagebegin + header + all slots < first data block (or new)
	return getFreeSpace() >= len;
}

Record SlottedPage::lookup(unsigned slotNumber)
{
	//doesn't exist (too high or null)
	if(slotNumber > header->slotCount-1 || (slots[slotNumber].offset == 0 && slots[slotNumber].length == 0) )
		return Record(0,NULL);
	return Record(slots[slotNumber].length,
		pagebegin + slots[slotNumber].offset);
}

unsigned SlottedPage::insert(unsigned len, const char* data)
{
	if(!isEnoughSpace(len)){
		return 0;
	}
	//find a free slot, the first with l=0 && offset==0
	unsigned i=0;
	while(slots[i].offset!=0 && slots[i].length!=0){
		i++;
	}
	if(header->slotCount < i+1)
		header->slotCount = i+1;
	//update the next free Space
	header->firstFreeSpace = header->firstFreeSpace - len;
	//store in table
	slots[i].offset = header->firstFreeSpace;
	slots[i].length = len;
	//and copy the data
	std::memcpy((void *) (pagebegin + slots[i].offset),
		data, len);
	return i; //return the index
}

bool SlottedPage::update(unsigned index, unsigned len, const char* data)
{
	if(!isEnoughSpace(len))
		return false;
	//entry does not exist
	if(index + 1 > header->slotCount){
		// too much slots requires
		if(!isEnoughSpace(len + (index + 1 - header->slotCount) * sizeof(struct Slot) ) )
			return false;
		//should be created, new index
		header->slotCount = index + 1;
		header->firstFreeSpace = header->firstFreeSpace - len;
		slots[index].offset = header->firstFreeSpace;
		slots[index].length = len;
		//and copy the data
		std::memcpy(
			(void*)(pagebegin + slots[index].offset),
			data, len);
		return true;
	//gets bigger
	}else if(len > slots[index].length){
		remove(index);
		header->firstFreeSpace = header->firstFreeSpace - len;
		slots[index].offset = header->firstFreeSpace;
		slots[index].length = len;
		//and copy the data
		std::memcpy(
			(void*)(pagebegin + slots[index].offset),
			data, len);
		return true;
	}else{ //no problem
		slots[index].length = len; //update only length
		std::memcpy(
			(void*) (pagebegin + slots[index].offset),
			data, len);
		return true;
	}
}
/** pos: begin of deleted entry
    len: how much move
*/
void SlottedPage::compactify(unsigned pos, unsigned len)
{
	if( pos < header->firstFreeSpace)
		return;
	// move data
	std::memmove(pagebegin + header->firstFreeSpace + len,
		pagebegin + header->firstFreeSpace,
		pos - header->firstFreeSpace);
	// set next free Space
	header->firstFreeSpace += len;
	// set slots: every slot before is now behind len
	for(unsigned i = 0; i < header->slotCount; i++)
		if(slots[i].offset <= pos)
			slots[i].offset += len;
}

bool SlottedPage::remove(unsigned index)
{
	if(index > header->slotCount - 1)
		return false;
	
	compactify(slots[index].offset, slots[index].length);
	slots[index].offset = 0;
	slots[index].length = 0;
	
	return true;
}

void SlottedPage::reset()
{
	header->lsn 		= 0;
	header->slotCount 	= 0;
	header->firstFreeSlot	= 0;
	header->dataStart	= PAGE_SIZE-1;
	header->firstFreeSpace	= header->dataStart;
}

void SlottedPage::print()
{
	std::cout << "LSN: " << header->lsn <<
		"; slotCount:" << header->slotCount <<
		"; firstFreeSpace:" << header->firstFreeSpace <<
		"; dataStart:" << header->dataStart <<
		"; freeAtAll:" << getFreeSpace() <<
		std::endl;
	for(unsigned i = 0; i < header->slotCount; i++){
		std::cout << i << "; offset" <<
			slots[i].offset << ": with len " <<
			slots[i].length << std::endl;
	}
}
