#ifndef TID_HPP
#define TID_HPP

#include <cstdint>

class TID{
	public:
		TID();
		TID(uint64_t);
		TID(uint64_t pageNo, uint64_t index);
		~TID();
		uint64_t getPageNo();
		unsigned getIndex();
		uint64_t getValue();
	private:
		uint64_t value;
};

#endif
