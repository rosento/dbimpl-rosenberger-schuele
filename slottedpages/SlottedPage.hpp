
#include <cstdint>
#include <cstdlib>

#include "../buffer/definitions.hpp"
#include "Record.hpp"

struct SPHeader{
	unsigned lsn;
	unsigned slotCount;		//Anzahl der Slots
	uint64_t firstFreeSpace;	//naechster Speicherplatz
	unsigned firstFreeSlot;		//naechster Slot
	uint64_t dataStart;
};

struct Slot{
	unsigned offset;
	unsigned length;
};

class SlottedPage{
	public:
		SlottedPage(void* page);
		~SlottedPage();
		bool isEnoughSpace(unsigned len);
		unsigned getFreeSpace();
		Record lookup(unsigned slotNumber);
		unsigned insert(unsigned len, const char* data);
		bool update(unsigned index, unsigned len, const char* data);
		bool remove(unsigned index);
		void compactify(unsigned pos, unsigned len);
		void reset();
		void print();

	private:
		struct SPHeader *header;
		struct Slot *slots;
		char *pagebegin;
};
