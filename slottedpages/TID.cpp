
#include "TID.hpp" 

TID::TID()
{
}

TID::TID(uint64_t v)
{
	value = v;
}

TID::TID(uint64_t pageNo, uint64_t index)
{
	value = pageNo << 8;
	value += index;
}

TID::~TID()
{

}

uint64_t TID::getPageNo()
{
	return value >> 8; 
}

unsigned TID::getIndex()
{
	return value & 0xFF; //last byte
}

uint64_t TID::getValue()
{
	return value;
}
