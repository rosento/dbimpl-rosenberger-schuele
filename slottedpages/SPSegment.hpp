#ifndef SPSEGMENT_HPP
#define SPSEGMENT_HPP

#include <cstdint>
#include <cstdlib>

#include "TID.hpp"
#include "../buffer/definitions.hpp"
#include "../buffer/BufferFrame.hpp"
#include "../buffer/BufferManager.hpp"
#include "SlottedPage.hpp"

//#define TID uint64_t

class SPSegment{
	public:
		SPSegment(uint64_t mySegment);
		SPSegment(uint64_t mySegment, BufferManager *);
		void init(uint64_t mySegment);
		~SPSegment();
		TID insert(const Record& r);
		bool remove(TID tid);
		Record lookup(TID tid);
		bool update(TID tid, const Record& r);
		void reset();
		unsigned getFreeSpaceOf(uint64_t pageID);
		void setFreeSpaceOf(uint64_t pageID, uint16_t value);
		uint64_t getMaxTID();

	private:

		uint16_t *freeSpaceInventory[4];
		BufferManager *bm;
		uint64_t firstPage;
		unsigned pagesForFreeSpace;
		uint64_t maxTID = 0;
};

#endif
