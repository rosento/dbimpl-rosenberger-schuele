#include <iostream>
#include <cstdlib>
#include <atomic>
#include <tbb/tbb.h>
#include <unordered_map>
#include <vector>
#include <mutex>

using namespace tbb;
using namespace std;

inline uint64_t hashKey(uint64_t k) {
   // MurmurHash64A
   const uint64_t m = 0xc6a4a7935bd1e995;
   const int r = 47;
   uint64_t h = 0x8445d61a4e774912 ^ (8*m);
   k *= m;
   k ^= k >> r;
   k *= m;
   h ^= k;
   h *= m;
   h ^= h >> r;
   h *= m;
   h ^= h >> r;
   return h|(1ull<<63);
}

class ChainingLockingHT {
   public:
   // Chained tuple entry
   struct Entry {
      uint64_t key;
      uint64_t value;
      Entry* next;
   };
   
   private:
   	uint64_t localsize;
	Entry** table;
	std::mutex** locks;
   public:

   // Constructor
   ChainingLockingHT(uint64_t size) {
   	localsize = size;

	// init and set pointers to null
	table = (Entry **) malloc(size*sizeof(Entry*));
	for(unsigned i = 0; i < size; i++)
		table[i] = NULL;

	//init mutexes for all chains
	locks = (std::mutex**) malloc(size*sizeof(std::mutex*));
	for(unsigned i = 0; i < size; i++)
		locks[i] = new std::mutex();
   }

   // Destructor
   ~ChainingLockingHT() {
   	free(table);
	
	for(unsigned i = 0; i < localsize; i++)
		delete locks[i];
	free(locks);
   }

   inline uint64_t lookup(uint64_t key) {
   	uint64_t pos = key % localsize;
	locks[pos]->lock(); //lock
	uint64_t ret = 0;
	for(Entry *cur = table[pos]; cur != NULL; cur = cur->next ){
		// look, if cur is equal to key
		if(cur->key == key)
			ret++;
	}
	locks[pos]->unlock(); //unlock
	return ret;
   }

   inline void insert(Entry* entry) {
   	uint64_t pos = entry->key % localsize;
	locks[pos]->lock(); //lock
	// set new entry to the front
	entry->next = table[pos];
	table[pos] = entry;
	locks[pos]->unlock(); //unlock
   }
};

class ChainingHT {
   public:
   // Chained tuple entry
   struct Entry {
      uint64_t key;
      uint64_t value;
      Entry* next;
   };

   //helper struct to have an atomic value
   struct Bucket{
	std::atomic<Entry*> first;
   };

   private:
   	uint64_t localsize;
	Bucket *table;

   public:

   // Constructor
   ChainingHT(uint64_t size) {
   	localsize = size;

	// init and set pointers to null
	table = (Bucket *) malloc(size*sizeof(Bucket));
	for(unsigned i = 0; i < size; i++)
		table[i].first = NULL;
   }

   // Destructor
   ~ChainingHT() {
   	free(table);
   }

   inline uint64_t lookup(uint64_t key) {
   	uint64_t pos = key % localsize;
	uint64_t ret = 0;

	// treat first separately, because atomic
	if(table[pos].first != NULL){
		if(table[pos].first.load()->key == key)
			ret++;

		for(Entry *cur = table[pos].first.load()->next; cur != NULL; cur = cur->next ){
			// look, if cur is equal to key
			if(cur->key == key)
				ret++;
		}
	}
	return ret;
   }

   inline void insert(Entry* entry) {
   	uint64_t pos = entry->key % localsize;
	entry->next = table[pos].first.load(std::memory_order_relaxed);
	//push to front
	while(! table[pos].first.compare_exchange_weak(entry->next, entry))
        	;
   }
};

class LinearProbingHT {
   public:
   // Entry
   struct Entry {
      uint64_t key;
//      uint64_t value;
      std::atomic<bool> marker;
   };

   private:
   std::vector<Entry> *entries;
   uint64_t size;

   public:

   // Constructor
   LinearProbingHT(uint64_t _size) {
	   size = 2 * _size;
	   Entry initial;
	   initial.marker.store(false);
	   entries = new std::vector<Entry>(size);
   }

   // Destructor
   ~LinearProbingHT() {
	   delete entries;
   }

   inline uint64_t lookup(uint64_t key) {
	   uint64_t hash = hashKey(key) % size;
	   uint64_t pos = hash;
	   uint64_t count = 0;

	   if ((*entries)[pos].marker.load() && (*entries)[pos].key == key) {
		   count++;
	   }

	   pos = (pos+1) % size;

	   // entry exists and no full circle yet
	   while ((*entries)[pos].marker.load() && pos != hash) {
		   if ((*entries)[pos].key == key) {
			   count++;
		   }

		   pos = (pos+1) % size;
	   }

	   return count;
   }

   inline void insert(uint64_t key) {
	   uint64_t hash = hashKey(key) % size;
	   uint64_t pos = hash;
	   bool expected = false;

	   // first entry free
	   if ((*entries)[pos].marker.compare_exchange_strong(expected, true)) {
		   (*entries)[pos].key = key;
		   return;
	   }

	   pos = (pos+1) % size;
	   expected = false;

	   // find free entry
	   while (pos != hash && !(*entries)[pos].marker.compare_exchange_strong(expected, true)) {
		   pos = (pos+1) % size;
		   expected = false;
	   }

	   if (pos != hash) {
		   (*entries)[pos].marker.store(true);
		   (*entries)[pos].key = key;
	   }
   }
};

int main(int argc,char** argv) {
   if(argc != 4){
   	std::cout << "Bitte genau 3 Parameter:" << std::endl << argv[0] << " " << "sizeR sizeS threadCount" << std::endl;
   	return 1;
   }
   uint64_t sizeR = atoi(argv[1]);
   uint64_t sizeS = atoi(argv[2]);
   unsigned threadCount = atoi(argv[3]);

   task_scheduler_init init(threadCount);

   // Init build-side relation R with random data
   uint64_t* R=static_cast<uint64_t*>(malloc(sizeR*sizeof(uint64_t)));
   parallel_for(blocked_range<size_t>(0, sizeR), [&](const blocked_range<size_t>& range) {
         unsigned int seed=range.begin();
         for (size_t i=range.begin(); i!=range.end(); ++i)
            R[i]=rand_r(&seed)%sizeR;
      });

   // Init probe-side relation S with random data
   uint64_t* S=static_cast<uint64_t*>(malloc(sizeS*sizeof(uint64_t)));
   parallel_for(blocked_range<size_t>(0, sizeS), [&](const blocked_range<size_t>& range) {
         unsigned int seed=range.begin();
         for (size_t i=range.begin(); i!=range.end(); ++i)
            S[i]=rand_r(&seed)%sizeR;
      });

   // STL
   {
      // Build hash table (single threaded)
      tick_count buildTS=tick_count::now();
      unordered_multimap<uint64_t,uint64_t> ht(sizeR);
      for (uint64_t i=0; i<sizeR; i++)
         ht.emplace(R[i],0);
      tick_count probeTS=tick_count::now();
      cout << "STL      build:" << (sizeR/1e6)/(probeTS-buildTS).seconds() << "MT/s ";

      // Probe hash table and count number of hits
      std::atomic<uint64_t> hitCounter;
      hitCounter=0;
      parallel_for(blocked_range<size_t>(0, sizeS), [&](const blocked_range<size_t>& range) {
            uint64_t localHitCounter=0;
            for (size_t i=range.begin(); i!=range.end(); ++i) {
               auto range=ht.equal_range(S[i]);
               for (unordered_multimap<uint64_t,uint64_t>::iterator it=range.first; it!=range.second; ++it)
                  localHitCounter++;
            }
            hitCounter+=localHitCounter;
         });
      tick_count stopTS=tick_count::now();
      cout << "probe: " << (sizeS/1e6)/(stopTS-probeTS).seconds() << "MT/s "
           << "total: " << ((sizeR+sizeS)/1e6)/(stopTS-buildTS).seconds() << "MT/s "
           << "count: " << hitCounter << endl;
   }

   // Test you implementation here... (like the STL test above)

   // chaining with locking
   {
      // Build hash table (single threaded)
      tick_count buildTS=tick_count::now();
      //unordered_multimap<uint64_t,uint64_t> ht(sizeR);
      ChainingLockingHT cht(sizeR);
      
      for (uint64_t i=0; i<sizeR; i++){
         ChainingLockingHT::Entry *entry = (ChainingLockingHT::Entry*) malloc(sizeof(ChainingLockingHT::Entry));
	 entry->key = R[i];
	 entry->value = 0;
         cht.insert(entry);
      }

      tick_count probeTS=tick_count::now();
      cout << "ChLockHT build:" << (sizeR/1e6)/(probeTS-buildTS).seconds() << "MT/s ";

      // Probe hash table and count number of hits
      std::atomic<uint64_t> hitCounter;
      hitCounter=0;
      parallel_for(blocked_range<size_t>(0, sizeS), [&](const blocked_range<size_t>& range) {
            uint64_t localHitCounter=0;
            for (size_t i=range.begin(); i!=range.end(); ++i) {
	       // add hits to current
	       localHitCounter += cht.lookup(S[i]);
            }
            hitCounter+=localHitCounter;
         });
      tick_count stopTS=tick_count::now();
      cout << "probe: " << (sizeS/1e6)/(stopTS-probeTS).seconds() << "MT/s "
           << "total: " << ((sizeR+sizeS)/1e6)/(stopTS-buildTS).seconds() << "MT/s "
           << "count: " << hitCounter << endl;
	   
   }

   // chaining without locking
   {
      // Build hash table (single threaded)
      tick_count buildTS=tick_count::now();
      //unordered_multimap<uint64_t,uint64_t> ht(sizeR);
      ChainingHT cht(sizeR);
      
      for (uint64_t i=0; i<sizeR; i++){
         ChainingHT::Entry *entry = (ChainingHT::Entry*) malloc(sizeof(ChainingHT::Entry));
	 entry->key = R[i];
	 entry->value = 0;
         cht.insert(entry);
      }

      tick_count probeTS=tick_count::now();
      cout << "ChHT     build:" << (sizeR/1e6)/(probeTS-buildTS).seconds() << "MT/s ";

      // Probe hash table and count number of hits
      std::atomic<uint64_t> hitCounter;
      hitCounter=0;
      parallel_for(blocked_range<size_t>(0, sizeS), [&](const blocked_range<size_t>& range) {
            uint64_t localHitCounter=0;
            for (size_t i=range.begin(); i!=range.end(); ++i) {
	       // add hits to current
	       localHitCounter += cht.lookup(S[i]);
            }
            hitCounter+=localHitCounter;
         });
      tick_count stopTS=tick_count::now();
      cout << "probe: " << (sizeS/1e6)/(stopTS-probeTS).seconds() << "MT/s "
           << "total: " << ((sizeR+sizeS)/1e6)/(stopTS-buildTS).seconds() << "MT/s "
           << "count: " << hitCounter << endl;
	   
   }


   // linear probing
   {
      // Build hash table (single threaded)
      tick_count buildTS=tick_count::now();
      LinearProbingHT pht(sizeR);
      
      for (uint64_t i=0; i<sizeR; i++){
         pht.insert(R[i]);
      }

      tick_count probeTS=tick_count::now();
      cout << "LinPrHT     build:" << (sizeR/1e6)/(probeTS-buildTS).seconds() << "MT/s ";

      // Probe hash table and count number of hits
      std::atomic<uint64_t> hitCounter;
      hitCounter=0;
      parallel_for(blocked_range<size_t>(0, sizeS), [&](const blocked_range<size_t>& range) {
            uint64_t localHitCounter=0;
            for (size_t i=range.begin(); i!=range.end(); ++i) {
	       // add hits to current
	       localHitCounter += pht.lookup(S[i]);
            }
            hitCounter+=localHitCounter;
         });
      tick_count stopTS=tick_count::now();
      cout << "probe: " << (sizeS/1e6)/(stopTS-probeTS).seconds() << "MT/s "
           << "total: " << ((sizeR+sizeS)/1e6)/(stopTS-buildTS).seconds() << "MT/s "
           << "count: " << hitCounter << endl;
	   
   }


   return 0;
}
