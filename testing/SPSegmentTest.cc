#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../buffer/BufferFrame.hpp"
#include "../buffer/Storage.hpp"
#include "../buffer/BufferManager.hpp"
#include "../slottedpages/SPSegment.hpp"
#include "gtest/gtest.h"

TEST(SPSegment, BasicStorage) {
	SPSegment sp(1);
	sp.reset();
	
	const char* data = "abcdefg";
	Record r(8,data);
	Record& rs = r;
	TID tid = sp.insert(rs);
	for(int i = 0; i < 1000; i++)
		sp.insert(rs);
	data = "hijklmo";
	Record r2(8,data);
	Record& rs2 = r2;
	TID tid2 = sp.insert(rs2);
//	for(int i = 0; i < PAGES; i++){
//		std::cout << sp.getFreeSpaceOf(i) << std::endl;
//	}
	ASSERT_STREQ("abcdefg", sp.lookup(tid).getData());
	ASSERT_STREQ("hijklmo", sp.lookup(tid2).getData());
}


