#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "gtest/gtest.h"

#include "../operators/Register.hpp"

TEST(Register, basic){
	Register r1(0), r2(0), r3(1), r4(-1), r5("bla"), r6("bla"), r7("blub"), r8();

	//of ints
	ASSERT_EQ(r1==r1, true);
	ASSERT_EQ(r1==r2, true);
	ASSERT_EQ(r1==r3, false);
	ASSERT_EQ(r1==r4, false);
	//of strings
	ASSERT_EQ(r5==r5, true);
	ASSERT_EQ(r5==r6, true);
	ASSERT_EQ(r5==r7, false);
}
