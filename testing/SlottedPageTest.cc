#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../buffer/BufferFrame.hpp"
#include "../buffer/Storage.hpp"
#include "../buffer/BufferManager.hpp"
#include "../slottedpages/SlottedPage.hpp"
#include "gtest/gtest.h"

TEST(SlottedPage, compactifyWorks) {
	BufferManager bm(4);
	BufferFrame& bf = bm.fixPage(3,true);
	void* data = bf.getData();
	uint64_t index[4];
	SlottedPage sp(data);
	sp.reset();
	const char *dataForRecord = (char*) malloc(4*sizeof(char));
	dataForRecord="aaa";
	index[0] = sp.insert(4,dataForRecord);
	dataForRecord="bbb";
	index[1] = sp.insert(4,dataForRecord);
	sp.remove(index[0]);
	ASSERT_STREQ("bbb",sp.lookup(index[1]).getData());
	bm.unfixPage(bf, false);
}

TEST(SlottedPage, insert) {
	BufferManager bm(4);
	BufferFrame& bf = bm.fixPage(1,true);
	ASSERT_EQ(1,bf.getPageNo());

	void* data = bf.getData();
	uint64_t index[4];

	SlottedPage sp(data);
	sp.reset();

	const char *dataForRecord = (char*) malloc(4*sizeof(char));
	dataForRecord="aaa";
	index[0] = sp.insert(4,dataForRecord);
	dataForRecord="bbb";
	index[1] = sp.insert(4,dataForRecord);
	dataForRecord="ccc";
	index[2] = sp.insert(4,dataForRecord);
	dataForRecord="ddd";
	index[3] = sp.insert(4,dataForRecord);
	ASSERT_STREQ("aaa",sp.lookup(index[0]).getData());
	ASSERT_STREQ("bbb",sp.lookup(index[1]).getData());
	ASSERT_STREQ("ccc",sp.lookup(index[2]).getData());
	ASSERT_STREQ("ddd",sp.lookup(index[3]).getData());

	sp.remove(2);
	ASSERT_STREQ("ddd",sp.lookup(index[3]).getData());

	bm.unfixPage(bf, false);
}

TEST(SlottedPage, update) {
	BufferManager bm(4);
	BufferFrame& bf = bm.fixPage(1,true);
	ASSERT_EQ(1,bf.getPageNo());

	void* data = bf.getData();

	SlottedPage sp(data);
	sp.reset();

	const char *dataForRecord = (char*) malloc(4*sizeof(char));
	dataForRecord="aab";
	sp.update(0,4,dataForRecord);
	dataForRecord="bbc";
	sp.update(1,4,dataForRecord);
	dataForRecord="dcc";
	sp.update(2,4,dataForRecord);
	dataForRecord="ccd";
	sp.update(3,4,dataForRecord);
	ASSERT_STREQ("aab",sp.lookup(0).getData());
	dataForRecord = (char*) malloc(8*sizeof(char));
	dataForRecord="abcdefg";
	sp.update(0,8,dataForRecord);
	ASSERT_STREQ("abcdefg",sp.lookup(0).getData());
	ASSERT_STREQ("bbc",sp.lookup(1).getData());
	ASSERT_STREQ("dcc",sp.lookup(2).getData());
	ASSERT_STREQ("ccd",sp.lookup(3).getData());

	bm.unfixPage(bf, false);
}

TEST(SlottedPage, isItSafe) {
	BufferManager *bm = new BufferManager(4);
	BufferFrame& bf = bm->fixPage(5 << BITS_PAGE ,true);

	void* data = bf.getData();

	SlottedPage *sp = new SlottedPage(data);
	sp->reset();

	const char *dataForRecord = (char*) malloc(4*sizeof(char));
	dataForRecord="aab";
	sp->update(0,4,dataForRecord);
	dataForRecord="bbc";
	sp->update(1,4,dataForRecord);
	dataForRecord="dcc";
	//close
	bm->unfixPage(bf, true);
	delete sp; 
	delete bm;
	
	bm = new BufferManager(4);
	BufferFrame& bff = bm->fixPage(5 << BITS_PAGE,true);
	data = bff.getData();
	sp = new SlottedPage(data);

	ASSERT_STREQ("aab",sp->lookup(0).getData());
	ASSERT_STREQ("bbc",sp->lookup(1).getData());

	bm->unfixPage(bf, false);
}

