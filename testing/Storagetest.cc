#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../buffer/BufferFrame.hpp"
#include "../buffer/Storage.hpp"
#include "../buffer/definitions.hpp"
#include "gtest/gtest.h"

TEST(Storagetest, all) {
	Storage st;
	char* data = (char*) malloc(PAGE_SIZE);
	char* data2 = (char*) malloc(PAGE_SIZE);
	
	//initialise for valgrind
	for(int i=0; i < PAGE_SIZE; i++){
		data[i]=0;
		data2[i]=0;
	}


	data[0]='a';
	data[1]='b';
	data[2]='c';
	data2[0]='d';
	data2[1]='e';
	data2[2]='f';
	st.writePage(0,0,(void*) data);
	st.writePage(1,0,(void*) data2);
	BufferFrame *bb = new BufferFrame(1);
	data = (char*) bb->getData();
//	printf("Der Wert ist: %c\n", data[0]);
	ASSERT_EQ('d',data[0]);

	bb = new BufferFrame(0);
	data = (char*) bb->getData();
//	printf("Der Wert ist: %c\n", data[0]);
	ASSERT_EQ('a',data[0]);
	st.deleteSegment(0);
}
TEST(Storagetest, bitOperationen) {
	Storage st;
	ASSERT_EQ(3,st.hintereKBits(255,2));
	ASSERT_EQ(127,st.vordereKBits(255,63));
	ASSERT_EQ(0xFFFFF,st.hintereKBits(0xFFFFFFFFFFFFFFFF,20));

//	std::cout << st.hintereKBits(255,2) << std::endl;
//	std::cout << st.vordereKBits(255,63) << std::endl;
}
