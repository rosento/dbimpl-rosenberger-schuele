#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../btree/BTree_impl.hpp"
#include "gtest/gtest.h"

/* Comparator functor for uint64_t*/
struct MyCustomUInt64Cmp {
bool operator()(uint64_t a, uint64_t b) const {
 return a<b;
}
};


TEST(BTree, ordered) {
	BufferManager *bm = new BufferManager(64);
	BTree<uint64_t,MyCustomUInt64Cmp> bt(4,bm);
	for(int i = 1; i < 20; i++){
		TID tid(i);
		bt.insert(i, tid);
	}
	TID tid1(1);
	ASSERT_EQ(19, bt.size());
	for(int i = 1; i < 20; i++){
		ASSERT_EQ(true,bt.lookup(i,tid1));
	}

}

TEST(BTree, theOtherWayRound) {
	BufferManager *bm = new BufferManager(64);
	BTree<uint64_t,MyCustomUInt64Cmp> bt(4,bm);
	for(int i = 20; i >=0; i--){
		TID tid(i);
		bt.insert(i, tid);
	}
	TID tid1(1);
	ASSERT_EQ(21, bt.size());
	for(int i = 0; i < 21; i++){
		ASSERT_EQ(true,bt.lookup(i,tid1));
	}
}
TEST(BTree, sthWeird) {
	BufferManager *bm = new BufferManager(64);
	BTree<uint64_t,MyCustomUInt64Cmp> bt(4,bm);
	TID tid(1);
	bt.insert(8, tid);
	bt.insert(4, tid);
	bt.insert(16, tid);
	bt.insert(32, tid);
	bt.insert(2, tid);
	bt.insert(5, tid);
	bt.insert(12, tid);
	bt.insert(31, tid);
	bt.insert(34, tid);
	bt.insert(18, tid);
	bt.insert(13, tid);
	ASSERT_EQ(11, bt.size());
}
TEST(BTree, deleteSth) {
	BufferManager *bm = new BufferManager(64);
	BTree<uint64_t,MyCustomUInt64Cmp> bt(4,bm);
	TID tid(1);
	for(int i = 0; i < 10; i++){
		TID tid(i);
		bt.insert(i, tid);
	}
	TID tid1(1);
	for(int i = 9; i >= 0; i--){
		bt.erase(i);
	}
}
TEST(BTree, eraseSth) {
	BufferManager *bm = new BufferManager(64);
	BTree<uint64_t,MyCustomUInt64Cmp> bt(4,bm);
	TID tid(1);
	for(int i = 0; i < 12; i++){
		TID tid(i);
		bt.insert(i, tid);
	}
	bt.erase(0);
	bt.erase(7);
	TID tid1(1);
	for(int i = 9; i >= 0; i--){
		bt.erase(i);
	}
}
TEST(BTree, onlyOne) {
	BufferManager *bm = new BufferManager(64);
	BTree<uint64_t,MyCustomUInt64Cmp> bt(4,bm);
	TID tid(1);
	bt.insert(0, tid);
	bt.erase(0);
}
