#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../buffer/BufferFrame.hpp"
#include "../buffer/Storage.hpp"
#include "../buffer/BufferManager.hpp"
#include "../buffer/definitions.hpp"
#include "gtest/gtest.h"

TEST(BufferManager, BasicInOut) {
	BufferManager bm(4);
	BufferFrame& bf = bm.fixPage(1,true);
	ASSERT_EQ(1,bf.getPageNo());
	BufferFrame& bff = bm.fixPage(2,false);
	
	char* mydata = (char*) bf.getData();

	mydata[0]='a';
	ASSERT_EQ('a',mydata[0]);
	ASSERT_EQ(2,bff.getPageNo());
	bm.unfixPage(bf,true);
	bm.unfixPage(bff,false);
}

TEST(BufferManager, PageFree) {
	BufferManager bm(4);
	BufferFrame& bfs0=bm.fixPage(1,true);
	BufferFrame& bfs1=bm.fixPage(2,false);
	BufferFrame& bfs2=bm.fixPage(3,false);
	BufferFrame& bfs3 = bm.fixPage(4,false);
	bm.unfixPage(bfs3,false);
	// einer zu viel...
	BufferFrame& bfs4=bm.fixPage(5,false);

	bm.unfixPage(bfs0,false);
	bm.unfixPage(bfs1,false);
	bm.unfixPage(bfs2,false);
	bm.unfixPage(bfs4,false);
}

TEST(BufferManager, AnotherSegment) {
	BufferManager bm(4);
	BufferFrame& bf = bm.fixPage(0xFFFFFFFFFFFFFFFF,true);
	ASSERT_EQ(0xFFFFFFFFFFFFFFFF,bf.getPageNo());
	char* mydata = (char*) bf.getData();
	mydata[0]='a';
	ASSERT_EQ('a',mydata[0]);
	bm.unfixPage(bf,true);
	// ueberpruefe Ergebnis
	BufferFrame& bff = bm.fixPage(0xFFFFFFFFFFFFFFFF,true);
	mydata = (char*) bff.getData();
	ASSERT_EQ('a',mydata[0]);
	bm.unfixPage(bff,true);
}
TEST(BufferManager, ExclusivAndShared) {
	BufferManager bm(4);
	BufferFrame& bf = bm.fixPage(0x1,true);
	bm.unfixPage(bf, false);
	BufferFrame& bf1 = bm.fixPage(0x1,false);
	BufferFrame& bf2 = bm.fixPage(0x1,false);
	bm.unfixPage(bf1, false);
	bm.unfixPage(bf2, false);
}

TEST(BufferManager, Exception) {
	BufferManager bm(1);
	BufferFrame& bf1 = bm.fixPage(0x1,true);
	try{
		bm.fixPage(0x2,false);
	}catch (std::exception& e){
		bm.unfixPage(bf1, false);
	}	
	
}
