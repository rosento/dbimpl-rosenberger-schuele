#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../buffer/BufferFrame.hpp"
#include "../buffer/TwoQueue.hpp"
#include "gtest/gtest.h"

TEST(TwoQueue, DummyElements) {
	TwoQueue tq;
	BufferFrame bf1,bf2;
	tq.add(&bf1);
	tq.add(&bf2);
	tq.again(&bf2);
	tq.remove();

}
TEST(TwoQueue, ExplicitPages) {
	TwoQueue tq;
	BufferFrame *bf1= new BufferFrame(0xFF);
	BufferFrame *bf2= new BufferFrame(0xF0);
	tq.add(bf1);
	tq.add(bf2);
	tq.again(bf2);
	tq.remove();
}
