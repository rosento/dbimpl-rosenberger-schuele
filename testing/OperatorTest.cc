#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "gtest/gtest.h"

#include "../operators/Register.hpp"
#include "../operators/DummyOP.hpp"
#include "../operators/SelectionOP.hpp"
#include "../operators/HashJoinOP.hpp"

TEST(Dummy, basic){
	DummyOP dummy(2,2);
	dummy.open();
	while(dummy.next()){
		std::cout << dummy.getOutput()[0]->getInteger() << std::endl;
	}
	dummy.close();
}

TEST(Selection, basic){
	DummyOP dummy(2,5);
	Register vergleich(2);
	SelectionOP select(&dummy, 0, vergleich);
	select.open();
	while(select.next()){
		std::cout << select.getOutput()[0]->getInteger() << std::endl;
	}
	select.close();
}

TEST(HashJoin, basic){
	DummyOP dummy(2,2);
	DummyOP dummy2(2,4);
	HashJoinOP hj(&dummy,&dummy2,0,0);

	hj.open();
	while(hj.next()){
		std::vector<Register *> erg = hj.getOutput();
		for(unsigned i = 0; i<erg.size(); i++)
			std::cout << erg[i]->getInteger() << " ";
		std::cout << std::endl;
	}
	hj.close();
}
