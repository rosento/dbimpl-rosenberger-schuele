#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include "../externalSort/ExternalSort.hpp"
#include "gtest/gtest.h"

int fdIn, fdOut, memSize;
uint64_t zaehler=0;


TEST(externalSort, DateiSortiert) {
	uint64_t last = 0, next = 0;

	std::cout << memSize << std::endl;
	/*sortieren nach fsOut*/
	externalSort(fdIn,zaehler,fdOut,memSize);
	
	/*einzeln Einlesen aus fdOut*/
	int i = 0;
	std::cerr << "Checking:" << std::endl;
	while( read(fdOut, &next, sizeof(uint64_t)) > 0){
		std::cout << i++ << std::endl;
		std::cout << "Naechste Zahl: " << next << std::endl;
		ASSERT_GE(next, last);
		last = next;
	}
	close(fdIn);
	close(fdOut);
}

int main(int argc, char **argv) {
  int localfile;
  uint64_t zahl;

  ::testing::InitGoogleTest(&argc, argv);

  if(argc!=4){
  	printf("Zu wenig Argumente:\n %s <inputFile> <outputFile> <memoryBufferInMB>\n", argv[0]);
	return 1;
  }

  /*oeffnen der Dateien, Dateideskriptoren speichern*/
  fdIn = open(argv[1],O_RDONLY);
  localfile = open(argv[1],O_RDONLY);
  fdOut = open(argv[2],O_RDWR);
  memSize = atoi(argv[3]);

  /*zu wenig Speicher*/
  if (memSize <= 0){
	printf("memoryBuffer muss groesser 0 sein");
	return 1;
  }

  /*zaehle Anzahl an zu sortierenden Zahlen*/
  while( read(localfile,&zahl, sizeof(uint64_t)) > 0 )
  	zaehler++;
  close(localfile);
  std::cout << "Es sind " << zaehler << " Zahlen zu sortieren" << std::endl;

  /*Tests ausfuehren*/
  return RUN_ALL_TESTS();
}
