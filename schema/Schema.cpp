#include "Schema.hpp"

#include <sstream>

static std::string type(const Schema::Relation::Attribute& attr) {
   Types::Tag type = attr.type;
   switch(type) {
      case Types::Tag::Integer:
         return "Integer";
      /*case Types::Tag::Numeric: {
         std::stringstream ss;
         ss << "Numeric(" << attr.len1 << ", " << attr.len2 << ")";
         return ss.str();
      }*/
      case Types::Tag::Char: {
         std::stringstream ss;
         ss << "Char(" << attr.len << ")";
         return ss.str();
      }
   }
   throw;
}

std::string Schema::toString() const {
   std::stringstream out;
   for (const Schema::Relation& rel : relations) {
      out << rel.name << std::endl;
      out << "\tPrimary Key:";
      for (unsigned keyId : rel.primaryKey)
         out << ' ' << rel.attributes[keyId].name;
      out << std::endl;
      for (const auto& attr : rel.attributes)
         out << '\t' << attr.name << '\t' << type(attr) << (attr.notNull ? " not null" : "") << std::endl;
   }
   return out.str();
}

unsigned Schema::Relation::Attribute::storedSize() {
	return sizeof(SAttribute) + name.size();
}

unsigned Schema::Relation::storedSize() {
	return sizeof(SRelation) + name.size() + attributes.size() * sizeof(TID) + primaryKey.size() * sizeof(TID);
}

void Schema::store(BufferManager* bm) {
	SPSegment segment(0, bm);
	char *data = (char *) malloc(relations.size() * sizeof(TID));
	Record root(relations.size() * sizeof(TID), data);
	TID *rels = (TID *) data;

	for (unsigned i = 0; i < relations.size(); i++) {
		rels[i] = relations[i].store(segment);
	}

	segment.update(0, root);

	free(data);
}

Schema::Schema() {
	//TODO Do anything here?
}

Schema::Schema(BufferManager* bm) {
	SPSegment segment(0, bm);
	Record root = segment.lookup(0);
	SSchema *sschema = (SSchema *) root.getData();
	TID *srels = (TID *) (root.getData() + sizeof(SSchema));

	for (unsigned i = 0; i * sizeof(TID) < sschema->relationsEnd; i++) {
		relations.emplace(relations.end(), segment, srels[i]);
	}
}

TID Schema::Relation::store(SPSegment segment) {
	char *data = (char *) malloc(storedSize());
	SRelation *srel = (SRelation *) data;

	// copy name
	for (srel->nameEnd = 0; name[srel->nameEnd]; srel->nameEnd++) {
		srel->name[srel->nameEnd] = name[srel->nameEnd];
	}
	srel->nameEnd++;
	name[srel->nameEnd] = 0;
	srel->nameEnd += sizeof(SRelation);

	// copy attributes
	TID *sattr = (TID *) (data + srel->nameEnd);
	for (srel->attributesEnd = 0; srel->attributesEnd < attributes.size(); srel->attributesEnd++) {
		sattr[srel->attributesEnd] = attributes[srel->attributesEnd].store(segment);
	}
	srel->attributesEnd = srel->attributesEnd * sizeof(TID) + srel->nameEnd;

	// copy primary key
	TID *prkey = (TID *) (data + srel->attributesEnd);
	for (srel->keyEnd= 0; srel->keyEnd < primaryKey.size(); srel->keyEnd++) {
		prkey[srel->attributesEnd] = attributes[srel->attributesEnd].store(segment);
	}
	srel->attributesEnd = srel->attributesEnd * sizeof(TID) + srel->nameEnd;


	Record rec(srel->attributesEnd, data);
	TID result = segment.insert(rec);
	free(data);
	return result;
}

Schema::Relation::Relation(SPSegment segment, TID tid) {
	Record rec = segment.lookup(tid);
	SRelation *srel = (SRelation *) rec.getData();
	TID *sattrs = (TID *) (rec.getData() + srel->nameEnd);

	name = *(new std::string(srel->name));

	// restore attributes
	for (int i = 0; i * sizeof(TID) + srel->nameEnd < srel->attributesEnd; i++) {
		attributes.emplace(attributes.end(), segment, sattrs[i]);
	}

	// restore key
	for (int i = 0; i * sizeof(TID) + srel->attributesEnd < srel->keyEnd; i++) {
		attributes.emplace(attributes.end(), segment, sattrs[i]);
	}
}

TID Schema::Relation::Attribute::store(SPSegment segment) {
	char *data = (char *) malloc(storedSize());
	SAttribute *sattr = (SAttribute *) data;

	// copy name
	for (sattr->nameEnd = 0; name[sattr->nameEnd]; sattr->nameEnd++) {
		sattr->name[sattr->nameEnd] = name[sattr->nameEnd];
	}
	sattr->nameEnd++;
	name[sattr->nameEnd] = 0;
	sattr->nameEnd += sizeof(SRelation);

	sattr->type = (unsigned) type;
	sattr->len = len;
	
	sattr->notNull = notNull;

        Record rec(sattr->nameEnd, data);
	TID result = segment.insert(rec);
	free(data);
	return result;
}

Schema::Relation::Attribute::Attribute(SPSegment segment, TID tid) {
	Record rec = segment.lookup(tid);
	SAttribute *sattr = (SAttribute *) rec.getData();
	
	// restore name
	name = *(new std::string(sattr->name));
	
	type = (Types::Tag) sattr->type;
	len = sattr->len;
	
	notNull = sattr->notNull;
}
