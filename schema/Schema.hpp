#ifndef H_Schema_hpp
#define H_Schema_hpp

#include <vector>
#include <string>
#include "Types.hpp"
#include "../slottedpages/SPSegment.hpp"

// Serialized Schema
struct SAttribute {
	unsigned nameEnd, type, len;
	bool notNull;
	char name[];
};
struct SRelation {
	unsigned nameEnd, attributesEnd, keyEnd;
	char name[];
};

struct SSchema {
	unsigned relationsEnd;
};

struct Schema {
   Schema();
   Schema(BufferManager *bm);
   struct Relation {
      Relation(SPSegment segment, TID tid);
      struct Attribute {
         Attribute(SPSegment segment, TID tid);
         std::string name;
         Types::Tag type;
         unsigned len;
         bool notNull;
         Attribute() : len(~0), notNull(true) {}
         unsigned storedSize();
         TID store(SPSegment segment);
      };
      std::string name;
      std::vector<Schema::Relation::Attribute> attributes;
      std::vector<unsigned> primaryKey;
      Relation(const std::string& name) : name(name) {}
      unsigned storedSize();
      TID store(SPSegment segment);
   };
   std::vector<Schema::Relation> relations;
   std::string toString() const;
   void store(BufferManager *bm);
};
#endif
