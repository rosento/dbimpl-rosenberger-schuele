#include <iostream>
#include <memory>
#include "Parser.hpp"
#include "Schema.hpp"

#define MEM_PAGES 10

int main(int argc, char* argv[]) {
   if (argc != 2) {
      std::cerr << "usage: " << argv[0] << " <schema file>" << std::endl;
      return -1;
   }

   Parser p(argv[1]);
   try {
      BufferManager bm(MEM_PAGES);
      std::unique_ptr<Schema> schema = p.parse();
      schema->store(&bm);

      //load schema
      Schema newSchema(&bm);
      *schema = newSchema;

      std::cout << schema->toString() << std::endl;
   } catch (ParserError& e) {
      std::cerr << e.what() << std::endl;
   }
   return 0;
}
