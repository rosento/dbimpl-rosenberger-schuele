#ifndef DBMS_HPP
#define DBMS_HPP

#include "../buffer/BufferManager.hpp"
#include "../buffer/BufferFrame.hpp"
#include "../buffer/definitions.hpp"

#include "../slottedpages/SPSegment.hpp"
//#include "../slottedpages/TID.hpp"
#include "../slottedpages/Record.hpp"

#include "../btree/BTree.hpp"

#endif
