all:
	mkdir -p bin
	(cd testing; make)
	(cd buffer; make)
	(cd hashjoin; make)

clean:
	(cd testing; make clean)
	(cd buffer; make clean)
	(cd hashjoin; make clean)
test:
	(cd bin; ./*Test)

.PHONY: all clean
